import { useRouter } from "next/dist/client/router";

const DeliveryIcon: React.FunctionComponent<{}> = (props) => {
    const router = useRouter();
    const active = (router.pathname.includes('/delivery'));
    const getColor = () => {
        if(active) {
            return '#ffffff'
        }
        else {
            return '#000'
        }
    }
    return (
        <div className='navigation'>
            <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 54.6 42.123" className='me-3'>
                <path fill={getColor()} id="Delivery_Truck_Icon" data-name="Delivery Truck Icon" d="M54.6,26.955a.9.9,0,0,0-.171-.632v-.211h0L49.311,13.476q-.512-1.264-1.536-1.264H35.831c-1.024,0-1.706.842-1.706,2.106V24.849H30.713V10.106C30.713,8.842,30.03,8,29.006,8H3.413c-1.024,0-1.706.842-1.706,2.106V24.849C.683,24.849,0,25.692,0,26.955V39.592C0,40.856.683,41.7,1.706,41.7H5.119c0,4.634,3.071,8.425,6.825,8.425s6.825-3.791,6.825-8.425H37.538c0,4.634,3.071,8.425,6.825,8.425s6.825-3.791,6.825-8.425h1.706c1.024,0,1.706-.842,1.706-2.106V26.955ZM37.538,16.425h9.214l3.413,8.425H37.538ZM13.65,24.849V20.637h5.119v4.212Zm8.531-4.212H27.3v4.212H22.181ZM27.3,16.425H22.181V12.212H27.3Zm-8.531,0H13.65V12.212h5.119ZM5.119,12.212h5.119v4.212H5.119Zm0,8.425h5.119v4.212H5.119Zm6.825,25.274c-1.877,0-3.413-1.9-3.413-4.212s1.536-4.212,3.413-4.212,3.413,1.9,3.413,4.212S13.821,45.911,11.944,45.911Zm32.419,0c-1.877,0-3.413-1.9-3.413-4.212s1.536-4.212,3.413-4.212,3.412,1.9,3.412,4.212S46.239,45.911,44.363,45.911Z" transform="translate(0 -8)"/>
            </svg>
        </div>
    )
}

export default DeliveryIcon;