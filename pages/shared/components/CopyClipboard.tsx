import { faCopy } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import React, { useEffect, useRef, useState } from "react"

export const CopyClipboard: React.FunctionComponent<{
    text: string
}> = (props) => {
    const [isShow, setIsShow] = useState<boolean>(false);
    const onCopyClick = () => {
        navigator.clipboard.writeText(props.text);

        setIsShow(true);
        setTimeout(() => {
            setIsShow(false);
        }, 1500);
    }
    const renderMsg = () => {
        if(isShow) {
            return (
                <small className='ms-1 text-primary position-absolute'>Text Copied!</small>
            )
        }

        return undefined;
    }
    return (
        <span onClick={onCopyClick} className='p-0 m-0'>
            <FontAwesomeIcon icon={faCopy} className='ms-2 text-primary my-auto pointer'></FontAwesomeIcon>
            {renderMsg()}
        </span>
    )
}
export default CopyClipboard;