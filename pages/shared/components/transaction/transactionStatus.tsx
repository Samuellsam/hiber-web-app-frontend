const TransactionStatus: React.FunctionComponent<{
    isPaid: boolean
}> = (props) => {
    const getStatusClassName = () => {
        if(props.isPaid) {
            return 'lunas-style';
        }

        return 'belum-lunas-style';
    }
    const getStatus = () => {
        if(props.isPaid) {
            return 'Lunas';
        }

        return 'Belum Lunas';
    }
    return (
        <div>
            <span className={"badge rounded-pill p-2 "+getStatusClassName()}>{getStatus()}</span>
        </div>
    );
}

export default TransactionStatus;