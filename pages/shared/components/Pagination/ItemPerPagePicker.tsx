import React from 'react';

export const ItemPerPagePicker: React.FunctionComponent<{
    onChange: (number: number) => void;
    defaultItemPerPage?: number;
    isAllowAll?: boolean;
}> = function ({ onChange, defaultItemPerPage = "5", isAllowAll = true }) {

    const onItemChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
        onChange(+e.target.value);
    };

    const renderAllOption = () => {
        if(isAllowAll === true){
            return <option value={0}>All</option>
        }else{
            return <></>
        }
    }

    return (
        <div className="d-flex flex-row px-4 py-2">
            <div className="d-flex align-items-center px-2">
                <p className="m-0">Item Per Halaman</p>
            </div>
            <div className="d-flex align-items-center px-2">
                <select className="form-select" defaultValue={defaultItemPerPage} onChange={onItemChange}>
                    {renderAllOption()}
                    <option value={5}>5</option>
                    <option value={10}>10</option>
                    <option value={15}>15</option>
                    <option value={20}>20</option>
                </select>
            </div>
        </div>
    );
}

export default ItemPerPagePicker;