import React, { useState, useEffect } from "react";
import Swal from "sweetalert2";
import { TransactionDetailViewModel, PurchaseOrderDetailViewModel, ReturProductClient, ReturProductDetail, UserClient, UserViewModel, PurchaseOrderClient, ReturPurchaseOrderDetail } from "../../../../api/hiber-api";
import Select from 'react-select'
import { DropdownModel } from "../../../transaction";

interface FormReturPurchaseOrderModel {
    productId: string;
    productName: string;
    maxQuantity: string;
    quantity: string;
    error: string;
}

const ReturPOModal: React.FunctionComponent<{
    purchaseOrderId: string,
    listPOProduct: PurchaseOrderDetailViewModel[],
    onRetur: () => void
}> = (props) => {
    const [isReturButtonDisabled, setIsReturButtonDisabled] = useState<boolean>(false);
    const [listReturPOProduct, setListReturPOProduct] = useState<FormReturPurchaseOrderModel[]>([]);
    const [note, setNote] = useState<string>('');

    useEffect(() => {
        initReturList();
    }, [props.listPOProduct]);

    const initReturList = () => {
        const listRetur = props.listPOProduct.map(Q => {
            return {
                productId: Q.productId,
                productName: Q.productName,
                maxQuantity: (Q.quantity - (Q.returQuantity ?? 0)).toString(),
                quantity: (0).toString(),
                error: ''
            } as FormReturPurchaseOrderModel
        });

        setListReturPOProduct([...listRetur]);
    }

    const updateNewListRetur = (value: string, index: number) => {
        let newList: FormReturPurchaseOrderModel[] = [];
        let thereIsError = false;
        listReturPOProduct.forEach((Q, currIndex) => {
            if(currIndex == index) {
                let newData = Q;
                newData.quantity = value;
                if(value == '' || parseInt(Q.quantity) < 0 || parseInt(Q.quantity) > (listReturPOProduct[index]?.maxQuantity ?? 0)) {
                    newData.error = 'Input Invalid';
                    thereIsError = true;
                }
                else {
                    newData.error = '';
                }
                newList.push({
                    ...newData
                });
            }
            else {
                newList.push({...Q});
            }
        });
        setListReturPOProduct([...newList]);
        if(thereIsError) {
            setIsReturButtonDisabled(true);
        }
        else {
            setIsReturButtonDisabled(false);
        }
    }

    const onInputChange = (e: React.ChangeEvent<HTMLInputElement>, index: number) => {
        const value = e.target.value;
        updateNewListRetur(value, index);
    }

    const renderReturForm = () => {
        if(listReturPOProduct != []) {
            const list = listReturPOProduct;
            const form = list.map((Q, index) => {
                return (
                    <div key={Q.productId} className='d-flex flex-column'>
                        <div className='flex-row d-flex justify-content-between mb-1'>
                            <p className='m-0 my-auto p-0 w-25'>{Q.productName}</p>
                            <p className='m-0 my-auto p-0 w-25'>{listReturPOProduct[index]?.maxQuantity}</p>
                            <input type="number" className='w-25' placeholder={Q.quantity} value={listReturPOProduct[index]?.quantity} onChange={(e) => onInputChange(e, index)}/>
                        </div>
                        <span className='ms-auto text-danger'>{listReturPOProduct[index]?.error}</span>
                        <hr />
                    </div>
                )
            });
            return form;
        }

        return undefined;
    }

    const resetForm = () => {
        initReturList();
        props.onRetur();
    }

    const retur = async () => {
        setIsReturButtonDisabled(true);
        try {
            const client = new PurchaseOrderClient();
            let productDatas: ReturPurchaseOrderDetail[] = [];
            listReturPOProduct.forEach(Q => {
                if(parseInt(Q.quantity) != 0) {
                    productDatas.push({
                        productId: Q.productId,
                        productQuantity: parseInt(Q.quantity)
                    });
                }
            })
            
            const success = await client.returPurchaseOrder({
                purchaseOrderId: props.purchaseOrderId,
                products: productDatas,
                note: note
            });
            if(success) {
                Swal.fire({
                    title: 'Berhasil melakukan retur',
                    text: 'Retur berhasil',
                    icon: 'success'
                });

                resetForm();
            }
            else {
                Swal.fire({
                    title: 'Gagal melakukan retur',
                    text: 'Retur gagal',
                    icon: 'error'
                });
            }
        } catch (error) {
            Swal.fire({
                title: 'Gagal melakukan retur',
                text: 'Retur gagal',
                icon: 'error'
            });
        } finally {
            setIsReturButtonDisabled(false);
        }
    }

    return (
        <div>
            <div className="modal fade" id="returPOStaticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex={-1} aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="staticBackdropLabel">Retur Purchase Order</h5>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" onClick={resetForm}></button>
                    </div>
                    <div className="modal-body">
                        <div className='flex-row d-flex justify-content-between'>
                            <p className='m-0 p-0 w-25 fw-bold'>Nama Barang</p>
                            <p className='m-0 p-0 w-25 fw-bold'>Max Retur</p>
                            <p className='m-0 p-0 w-25 fw-bold'>Jumlah Retur</p>
                        </div>
                        <hr />
                        {renderReturForm()}
                        <br />
                        <div className='d-flex flex-row justify-content-between mb-1'>
                            <p className='m-0 my-auto p-0'>Notes</p>
                            <div className='w-50'>
                                <textarea onChange={(e) => setNote(e.target.value)} className='d-flex w-100' value={note}/>
                            </div>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal" onClick={resetForm}>Batal</button>
                        <button type="button" className="btn btn-primary" disabled={isReturButtonDisabled} onClick={retur} data-bs-dismiss="modal">Retur</button>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ReturPOModal;