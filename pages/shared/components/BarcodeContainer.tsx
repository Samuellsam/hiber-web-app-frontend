import JsBarcode from "jsbarcode";
import { useEffect, useState } from "react";

const BarcodeContainer: React.FunctionComponent<{
    type: string,
    id: string,
    value?: string
}> = (props) => {
  const [successCode, setSuccessCode] = useState('');
  const renderBarcode = (val: string, color: string = '#00000') => {
    JsBarcode('#'+props.id, val, {
      format: props.type,
      lineColor: color,
      width:2,
      height:40,
      displayValue: true
    });
  }
  useEffect(() => {
    let err = false;
    if(props.value){
        try{
            renderBarcode(props.value);
          }
          catch(e) {
            err = true;
            if(successCode != '') {
              renderBarcode(successCode, '#00000');
            }
          }
          if(!err) {
            setSuccessCode(props.value);
          }
    }
    
  }, [props.value]);

  return (
    <div className='mx-auto'>
      <svg className='mx-auto' id={props.id}/>
    </div>
  );
}

  export default BarcodeContainer;