import { useEffect, useState } from "react"

export enum ReturnType {
    STRING = 1,
    NUMBER,
    EVENT 
}

const PriceInput: React.FunctionComponent<{
    id?: string,
    value: number,
    onChange?: (value) => void,
    returnType?: ReturnType,
    className?: string
}> = (props) => {
    const [value, setValue] = useState('');

    useEffect(() => {
        if(isNaN(props.value) == false) {
            setValue(generateNewValue(props.value.toString()));
        }
    }, [props.value]);

    const generateNewValue = (text: string) => {
        //apus semua titik
        text = text.replaceAll('.', '');
        
        for(let i=text.length-4; i>=0; i-=3) {
            let subLeft = text.slice(0, i+1);
            let subRight = text.slice(i+1);
            text = subLeft+'.'+subRight;
        }

        return text;
    }

    const onPriceChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newValue = e.target.value;
        
        if(newValue.match('[^.0-9]+')) return;

        const generatedPrice = generateNewValue(newValue); 
        setValue(generatedPrice);
        
        if(props.onChange) {
            if(props.returnType == ReturnType.STRING) {
                props.onChange(generatedPrice.replaceAll('.', ''));
            }
            else if(props.returnType == ReturnType.NUMBER) {
                props.onChange(parseInt(generatedPrice.replaceAll('.', '')));
            }
            else if(props.returnType == ReturnType.EVENT) {
                const newE = e;
                newE.target.value = generatedPrice.replaceAll('.', '');
                props.onChange(newE);
            }
        }
    }
    return (
        <div>
            <input id={props.id} type="text" onChange={onPriceChange} className={props.className} value={value}/>
        </div>
    )
}

export default PriceInput;