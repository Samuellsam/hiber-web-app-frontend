const LoadingPage: React.FunctionComponent<{
    loading: boolean
}> = (props) => {
    return (
        <div>
            {props.loading ? 
                <div  className='position-fixed w-100 h-100 start-50 top-50 translate-middle' style={{zIndex: 10}}>
                    <div className="d-flex flex-row justify-content-center p-5 card shadow-lg position-fixed bg-light w-100 h-100 start-50 top-50 translate-middle" style={{opacity: 0.9}}>
                    </div>
                    <div className='position-fixed start-50 top-50 translate-middle d-flex flex-row card bg-light px-5 py-4 shadow-lg' style={{zIndex: 11}}>
                        <div className="spinner-border my-auto" role="status">
                            <span className="sr-only"></span>
                        </div>
                        <h5 className='my-auto ms-3'>Mohon Tunggu...</h5>
                    </div>
                </div> : undefined}
        </div>
    )
}

export default LoadingPage;