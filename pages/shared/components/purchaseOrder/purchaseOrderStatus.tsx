import { PurchaseOrderStatusEnum } from "../../../../enums/enums";

const PurchaseOrderStatus: React.FunctionComponent<{
    status: number
}> = (props) => {
    const getStatusClassName = () => {
        if(props.status === PurchaseOrderStatusEnum.WAITING_FOR_APPROVAL) {
            return 'menunggu-persetujuan-style';
        } else if (props.status === PurchaseOrderStatusEnum.DECLINED) {
            return 'ditolak-style';
        } else if(props.status === PurchaseOrderStatusEnum.APPROVED){
            return 'disetujui-style'
        } else if(props.status === PurchaseOrderStatusEnum.PENGAJUAN_RETUR){
            return 'retur-style'
        } else if(props.status === PurchaseOrderStatusEnum.SELESAI){
            return 'selesai-style'
        }

        return '';
    }

    const getStatus = () => {
        if(props.status == PurchaseOrderStatusEnum.WAITING_FOR_APPROVAL) {
            return 'Menunggu Persetujuan';
        }
        else if(props.status == PurchaseOrderStatusEnum.DECLINED) {
            return 'Ditolak';
        }
        else if(props.status == PurchaseOrderStatusEnum.APPROVED) {
            return 'Disetujui';
        }
        else if(props.status == PurchaseOrderStatusEnum.PENGAJUAN_RETUR) {
            return 'Retur';
        }
        else if(props.status == PurchaseOrderStatusEnum.SELESAI) {
            return 'Selesai';
        }

        return '';
    }
   
    return (
        <div>
            <span className={"badge rounded-pill p-2 "+getStatusClassName()}>{getStatus()}</span>
        </div>
    );
}

export default PurchaseOrderStatus;