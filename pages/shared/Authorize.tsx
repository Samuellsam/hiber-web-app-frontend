import { useRouter } from "next/dist/client/router";
import React, { useEffect, useState } from "react";
import { useCookies } from "react-cookie"
import ForbiddenPage from "./Forbidden";

const Authorize: React.FunctionComponent<{
    roleId?: number | undefined,
    onlyLoggedIn?: boolean | undefined
}> = (props) => {
    const [cookies] = useCookies(['username', 'userId', 'roleName', 'roleId']);
    const [isShowed, setIsShowed] = useState<boolean>(false);
    const [isProcessing, setIsProcessing] = useState<boolean>(true);
    const router = useRouter();
    const checkIsCookieExist = () => {
        if(cookies.username != null && cookies.userId != null && cookies.roleName != null && cookies.roleId != null) {
            checkIsAutheticated();
        }
        else {
            router.push('/login');
        }
    }

    const checkIsAutheticated = () => {
        if(props.roleId != undefined && cookies.roleId == props.roleId) {
            setIsShowed(true);
        }
        setIsProcessing(false);
    }

    const checkIsLoggedIn = () => {
        if(cookies.username == null || cookies.userId == null || cookies.roleName == null || cookies.roleId == null) {
            router.push('/login');
        }
        else {
            setIsShowed(true);
            setIsProcessing(false);
        }
    }

    useEffect(() => {
        if(props.onlyLoggedIn == true) {
            checkIsLoggedIn();
        }
        else {
            checkIsCookieExist();
        }
    }, []);

    return (
        <div className='w-100 h-100'>
            {isProcessing ? undefined : 
                isShowed ? props.children : <ForbiddenPage/>
            }
        </div>
    )
}

export default Authorize;