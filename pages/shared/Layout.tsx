import Head from "next/head";
import React from "react";
import Menu from "./Menu";
import TopBar from "./TopBar";

const Layout: React.FunctionComponent<{
    title: string
}> = (props) => {
    return (
        <div>
            <Head>
                <meta charSet="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <title>{props.title} - HiBer Web App</title>
            </Head>
            <main>
                <TopBar></TopBar>
                <div>
                    <div className="navbar-container">
                        <Menu></Menu>
                    </div>
                    <div className="content-container p-3">
                        {props.children}
                    </div>
                </div>
            </main>
        </div>
    );
}

export default Layout;