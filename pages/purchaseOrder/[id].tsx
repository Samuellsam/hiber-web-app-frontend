import moment from "moment";
import { GetServerSideProps } from "next"
import Link from "next/link";
import React, { useEffect, useState } from "react"
import { useCookies } from "react-cookie";
import { DetailViewModel, PdfClient, PurchaseOrderClient, PurchaseOrderDetailViewModel } from "../../api/hiber-api";
import PurchaseOrderStatus from "../shared/components/purchaseOrder/purchaseOrderStatus";
import Layout from "../shared/Layout"
import 'moment/locale/id';
import Swal from "sweetalert2";
import { PurchaseOrderStatusEnum, RoleEnum } from "../../enums/enums";
import ReturPOModal from "../shared/components/retur/returPOModal";
import ReportPOModal from "../shared/components/retur/reportPOModal";
import Authorize from "../shared/Authorize";
import LoadingPage from "../shared/components/LoadingPage";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faDownload } from "@fortawesome/free-solid-svg-icons";
import PriceFormat from "../shared/components/PriceFormat";
import BreadCrumbs from "../shared/components/BreadCrumbs";

var idLocale = require('moment/locale/id');
moment.locale('id', idLocale);

const PurchaseOrderDetail: React.FunctionComponent<{
    id: string
}> = (props) => {

    const [detail, setDetail] = useState<DetailViewModel>({
        purchaseOrder: {
            purchaseOrderId: '',
            supplierName: '',
            submitBy: '',
            totalPrice: 0,
            status: '',
            createdAt: '',
            purchaseOrderStatusId: 0,
            note: ''
        },
        purchaseOrderDetail: []
    });
    const [cookies, setCookie, removeCookie] = useCookies(['username', 'userId', 'roleId', 'roleName']);
    const [isSubmitting, SetIsSubmitting] = useState<boolean>(false);
    const [roleId, setRoleId] = useState<number>(-1);
    useEffect(() => {
        setRoleId(cookies.roleId);
        fetchData();
    }, [])

    const fetchData = async () => {
        const client = new PurchaseOrderClient
        SetIsSubmitting(true);
        try {
            const data: DetailViewModel = await client.getPurchaseOrderDetail(props.id);
            setDetail(data);
        } catch (e) {

        } finally {
            SetIsSubmitting(false);
        }
    }

    const changePurchaseOrderStatus = async (action: string) => {
        var statusId = 0
        if(action == "Reject") {
            statusId = PurchaseOrderStatusEnum.DECLINED;
        } else if(action == "Approve") {
            statusId = PurchaseOrderStatusEnum.APPROVED
        } else if(action == "Retur") {
            statusId = PurchaseOrderStatusEnum.PENGAJUAN_RETUR
        } else if(action == "Selesai") {
            statusId = PurchaseOrderStatusEnum.SELESAI
        }

        let response;
        if(action == "Selesai") {
            response = await Swal.fire({
                title: "Selesaikan Purchase Order?",
                html: '<span>Apakah anda yakin ingin menyelesaikan PO?</span> <br/> <br/><span class="text-danger">* Aksi ini tidak dapat dikembalikan</span>',
                showCancelButton: true,
                focusConfirm: false,
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak"
            });
        }

        if((action == "Selesai" && response.isConfirmed) || action != 'Selesai') {
            SetIsSubmitting(true);
            const client = new PurchaseOrderClient
            try {
                const response = await client.updatePurchaseOrderStatus(props.id,statusId);
                Swal.fire({
                    title: 'Berhasil ' + action + ' purchase order',
                    text: 'Purchase order telah diubah',
                    icon: 'success'
                });
            } catch (er) {
                Swal.fire({
                    title: 'Gagal ' + action + ' purchase order',
                    text: 'Purchase order gagal diubah',
                    icon: 'error'
                });
            } finally {
                await fetchData();
                SetIsSubmitting(false);
            }
        }
        
        SetIsSubmitting(false);
    }

    const renderPurchaseRequests = () => {
        const list: PurchaseOrderDetailViewModel[] = detail.purchaseOrderDetail as PurchaseOrderDetailViewModel[];
        return list.map(Q =>
            <tr key={Q.productName}>
                <td>
                    <p>{Q.productName}</p>
                </td>
                <td>
                    <p>{Q.quantity}</p>
                </td>
                <td>
                    <p>{Q.returQuantity}</p>
                </td>
                <td>
                    <p><PriceFormat value={Q.price}/></p>
                </td>
            </tr>);
    }
    
    const base64ToArrayBuffer = (base64: string) => {
        var binary_string = window.atob(base64);
        var len = binary_string.length;
        var bytes = new Uint8Array(len);
        for (var i = 0; i < len; i++) {
            bytes[i] = binary_string.charCodeAt(i);
        }
        return bytes.buffer;
    }

    const printPOFile = async () => {
        const client = new PdfClient();
        try {
            SetIsSubmitting(true);
            const resultPO = await client.generatePurchaseOrderFile(props.id);
            var blobInstallment = new Blob([base64ToArrayBuffer(resultPO)], { type: 'application/pdf' });
            var blobInstallmentURL = URL.createObjectURL(blobInstallment);
            window.open(blobInstallmentURL);

            Swal.fire({
                title: 'Berhasil mendownload template',
                text: 'Template purchase order berhasil di download',
                icon: 'success'
            });
        } catch (error) {
            Swal.fire({
                title: 'Gagal mendownload template',
                text: 'Template purchase order gagal di download',
                icon: 'error'
            });
        } finally {
            SetIsSubmitting(false);
        }
    }

    const getButtonSet = () => {
        if(detail.purchaseOrder) {
            if (roleId == RoleEnum.ADMIN && detail.purchaseOrder.purchaseOrderStatusId == PurchaseOrderStatusEnum.WAITING_FOR_APPROVAL) {
                return <div>
                    <button className='btn btn-danger my-auto me-2' onClick={() => changePurchaseOrderStatus("Reject")} disabled={isSubmitting}>Reject</button>
                    <button className='btn btn-primary my-auto' onClick={() =>changePurchaseOrderStatus("Approve")} disabled={isSubmitting}>Approve</button>
                </div>;
            }
            else if (roleId == RoleEnum.ADMIN && detail.purchaseOrder.purchaseOrderStatusId == PurchaseOrderStatusEnum.APPROVED) {
                return <div>
                    <button className='btn btn-info my-auto me-2 text-light' type="button" data-bs-toggle="modal" data-bs-target="#returPOStaticBackdrop" disabled={isSubmitting}>Retur</button>
                    <button className='btn btn-danger my-auto me-2 text-light' type="button" data-bs-toggle="modal" data-bs-target="#reportPOStaticBackdrop" disabled={isSubmitting}>Barang Rusak</button>
                    <button className='btn btn-warning my-auto text-light' onClick={() =>changePurchaseOrderStatus("Selesai")} disabled={isSubmitting}>Selesai</button>
                    <button className='btn btn-success my-auto ms-2' onClick={printPOFile}><FontAwesomeIcon icon={faDownload} className='me-2'></FontAwesomeIcon> Cetak Purchase Order</button>
                </div>;
            }
            else if (roleId == RoleEnum.ADMIN && detail.purchaseOrder.purchaseOrderStatusId == PurchaseOrderStatusEnum.PENGAJUAN_RETUR) {
                return <div>
                    <button className='btn btn-warning my-auto text-light' onClick={() =>changePurchaseOrderStatus("Selesai")} disabled={isSubmitting}>Selesai</button>
                    <button className='btn btn-success my-auto ms-2' onClick={printPOFile}><FontAwesomeIcon icon={faDownload} className='me-2'></FontAwesomeIcon> Cetak Purchase Order</button>
                </div>;
            }
            else {
                return <div>
                    <Link href='/purchaseOrder'><a type="button" className='btn btn-danger my-auto'>Tutup</a></Link>
                    <button className='btn btn-success my-auto ms-2' onClick={printPOFile}><FontAwesomeIcon icon={faDownload} className='me-2'></FontAwesomeIcon> Cetak Purchase Order</button>
                </div>;
            }
        }

        return undefined;
    }

    return (
        <div>
            <BreadCrumbs href="/purchaseOrder" isChanged={false} text="Kembali ke Daftar Purchase Order"/>
            <h3 className="mb-4">Detail Purchase Order</h3>
            <hr />
            <LoadingPage loading={isSubmitting}/>
            <ReportPOModal listPOProduct={detail.purchaseOrderDetail ?? []} onReport={fetchData} purchaseOrderId={props.id}></ReportPOModal>
            <ReturPOModal listPOProduct={detail.purchaseOrderDetail ?? []} onRetur={fetchData} purchaseOrderId={props.id}></ReturPOModal>
            <div className='row'>
                <div className='col-3'>
                    <p>Supplier</p>
                </div>
                <div className='col-9'>
                    <p>{detail.purchaseOrder?.supplierName}</p>
                </div>
            </div>
            <div className='row'>
                <div className='col-3'>
                    <p>Request Tanggal</p>
                </div>
                <div className='col-9'>
                    <p>{moment(detail.purchaseOrder?.createdAt).format("D MMMM YYYY")}</p>
                </div>
            </div>
            <div className='row'>
                <div className='col-3'>
                    <p>Status</p>
                </div>
                <div className='col-9'>
                    <PurchaseOrderStatus status={detail.purchaseOrder?.purchaseOrderStatusId ?? 0} />
                </div>
            </div>
            <div className='row'>
                <div className='col-3'>
                    <p>Retur Note</p>
                </div>
                <div className='col-9'>
                    <p>{detail.purchaseOrder?.note ?? '-'}</p>
                </div>
            </div>
            <br />
            <div>
                <p className='text-center fw-bold'>Purchase Request</p>
                <table className='table table-striped m-0 text-center table-hover'>
                    <thead className='table-header'>
                        <tr>
                            <th>Nama Produk</th>
                            <th>Kuantitas</th>
                            <th>Kuantitas Retur</th>
                            <th>Harga</th>
                        </tr>
                    </thead>
                    <tbody>
                        {renderPurchaseRequests()}
                    </tbody>
                </table>
            </div>
            <br />
            <br />
            <div className='d-flex'>
                <div className='ms-auto my-auto'>Total Harga: <PriceFormat value={detail.purchaseOrder?.totalPrice}/></div>
                <div className="col-1"></div>
                {getButtonSet()}
            </div>
        </div>
    )
}

const PurchaseOrderDetailPage: React.FunctionComponent<{
    id: string
}> = (props) => {
    return (
        <Authorize onlyLoggedIn={true}>
            <Layout title='Purchase Order Detal'>
                <PurchaseOrderDetail id={props.id}></PurchaseOrderDetail>
            </Layout>
        </Authorize>
    )
}

export const getServerSideProps: GetServerSideProps<{
    id: string
}> = async (context) => {
    if (context.params) {
        const id = context.params['id'];

        if (typeof id === 'string') {

            return {
                props: {
                    id: id
                }
            }
        }
    }

    return {
        props: {
            id: ''
        }
    };
}

export default PurchaseOrderDetailPage;