import Link from "next/link"
import React, { useEffect, useState } from "react"
import Layout from "../shared/Layout"
import { DateRangePicker, FocusedInputShape } from 'react-dates';
import Select from 'react-select';
import { Moment } from "moment";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEraser, faFilter, faInfoCircle, faPlus, faSearch } from "@fortawesome/free-solid-svg-icons";
import { PurchaseOrderClient, PurchaseOrderDataGridModel, PurchaseOrderViewModel } from "../../api/hiber-api";
import PurchaseOrderStatus from "../shared/components/purchaseOrder/purchaseOrderStatus";
import { PaginationValue } from "../../interface/IPagination";
import { Pagination } from "../shared/components/Pagination/Pagination";
import { ItemPerPagePicker } from "../shared/components/Pagination/ItemPerPagePicker";
import Authorize from "../shared/Authorize";
import LoadingPage from "../shared/components/LoadingPage";
import PriceFormat from "../shared/components/PriceFormat";

interface DropdownModel {
    label: string;
    value: number | undefined;
}

interface FilterModel {
    supplierName: string;
    submitBy: string;
    statusId: DropdownModel;
    dateFrom: Moment | null;
    dateTo: Moment | null;
}

const PurchaseOrderGridData: React.FunctionComponent<{
    purchaseOrders: PurchaseOrderViewModel[]
}> = (props) => {
    const renderRowData = () => {
        return props.purchaseOrders.map(Q =>
            <tr key={Q.purchaseOrderId}>
                <td>
                    <p>{Q.supplierName}</p>
                </td>
                <td>
                    <p>{Q.submitBy}</p>
                </td>
                <td>
                    <PriceFormat value={Q.totalPrice}/>
                </td>
                <td>
                    <PurchaseOrderStatus status={Q.purchaseOrderStatusId} />
                </td>
                <td>
                    <Link href={'/purchaseOrder/' + Q.purchaseOrderId}>
                        <a className='btn btn-primary'> <FontAwesomeIcon icon={faInfoCircle} className='mx-1 text-light'></FontAwesomeIcon>Detail</a>
                    </Link>
                </td>
            </tr>
        );
    }

    return (
        <div>
            <table className='table table-striped m-0 text-center table-hover'>
                <thead className='table-header'>
                    <tr>
                        <th>Nama Supplier</th>
                        <th>Diajukan Oleh</th>
                        <th>Total Harga</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {renderRowData()}
                </tbody>
            </table>

        </div>
    )
}

const PurchaseOrder: React.FunctionComponent<{}> = (props) => {

    const [listStatus, setListStatus] = useState<DropdownModel[]>([
        {
            label: 'Waiting for Approval',
            value: 1
        },
        {
            label: 'Decline',
            value: 2
        },
        {
            label: 'Approved',
            value: 3
        },
        {
            label: 'Retur',
            value: 4
        },
        {
            label: 'Selesai',
            value: 5
        },
    ]);
    const [purchaseOrderList, setPurchaseOrderList] = useState<PurchaseOrderViewModel[]>([])
    const [isFilterShowed, setIsFilterShowed] = useState<boolean>(false);
    const [currPage, setCurrPage] = useState<number>(1);
    const [maxPage, setMaxPage] = useState<number>(1);
    const [focused, setFocused] = useState<FocusedInputShape | null>(null);
    const [pagination, setPagination] = useState<PaginationValue>({
        currentPage: 1,
        itemPerPage: 5,
        totalData: 0,
    });

    const [filter, setFilter] = useState<FilterModel>({
        supplierName: '',
        submitBy: '',
        dateFrom: null,
        dateTo: null,
        statusId: {
            value: undefined,
            label: ''
        },
    });
    const [isLoading, setIsLoading] = useState<boolean>(true);

    const changeFilterState = () => {
        setIsFilterShowed(!isFilterShowed);
    }

    const onSupplierNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        const newFilter = filter;
        newFilter.supplierName = value;
        setFilter({
            ...newFilter
        });
    }

    const onSubmitByChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        const newFilter = filter;
        newFilter.submitBy = value;
        setFilter({
            ...newFilter
        });
    }

    const onStatusChange = (selected) => {
        const newFilter = filter;
        newFilter.statusId = selected;
        setFilter({
            ...newFilter
        });
    }

    const resetFilter = () => {
        const newPagination = {
            ...pagination,
            currentPage: 1
        };
        const newFilter = {
            supplierName: '',
            dateFrom: null,
            dateTo: null,
            statusId: {
                value: undefined,
                label: ''
            },
            submitBy: ''
        };

        setFilter({...newFilter});
        setPagination({...newPagination});

        try {
            fetchData(newPagination, newFilter);
        } catch (error) {
            
        }
    }

    const searchFilteredData = async () => {
        const newPagination = {
            ...pagination,
            currentPage: 1
        };

        setPagination({...newPagination});
        try {
            fetchData(newPagination, filter);
        } catch (error) {
            
        }
    }

    useEffect(() => {
        fetchData(pagination, filter);
    }, [])

    const fetchData = async (pagination: PaginationValue, filterData: FilterModel) => {
        setIsLoading(true);
        const purchaseOrderClient = new PurchaseOrderClient();
        try {
            const data: PurchaseOrderDataGridModel = await purchaseOrderClient.getAllPurchaseOrder(
                filter.dateFrom == null ? undefined : filter.dateFrom.toISOString(),
                filter.dateTo == null ? undefined : filter.dateTo.toISOString(),
                filter.statusId.value == 0 ? undefined : filter.statusId.value,
                filter.supplierName,
                filter.submitBy,
                pagination.currentPage,
                pagination.itemPerPage);

            setPagination({...pagination, totalData: data.totalData})
            setPurchaseOrderList(data.purchaseOrders as PurchaseOrderViewModel[]);
        } catch (e) {
        } finally {
            setIsLoading(false);
        }
    }

    const updatePaginate = async (itemPerPage: number, currentPage: number) => {
        const paginate = pagination;
        paginate.itemPerPage = itemPerPage;
        paginate.currentPage = currentPage;
        setPagination(paginate);

        fetchData(paginate, filter);
    }

    const renderFilterForm = () => {
        if (isFilterShowed) {
            return (
                <div>
                    <div className='px-3'>
                        <div className='d-flex flex-row justify-content-between my-1'>
                            <p className='w-50 my-auto'>Nama Supplier</p>
                            <input type="text" value={filter.supplierName} className='form-control w-50' placeholder='Search' onChange={onSupplierNameChange} />
                        </div>
                        <div className='d-flex flex-row justify-content-between my-1'>
                            <p className='w-50 my-auto'>Diajukan Oleh</p>
                            <input type="text" value={filter.submitBy} className='form-control w-50' placeholder='Search' onChange={onSubmitByChange} />
                        </div>
                        <div className='d-flex flex-row justify-content-between mb-1'>
                            <p className='w-50 my-auto'>Status</p>
                            <div className='w-50'>
                                <Select options={listStatus} onChange={onStatusChange} value={filter.statusId} />
                            </div>
                        </div>
                        <div className='d-flex flex-row justify-content-between mb-1'>
                            <p className='w-50 my-auto'>Tanggal Request</p>

                            <div className='w-50'>
                                <DateRangePicker
                                    small={true}
                                    block={true}
                                    isOutsideRange={() => false}
                                    startDatePlaceholderText={'Tanggal awal'}
                                    endDatePlaceholderText={'Tanggal akhir'}
                                    showClearDates={true}
                                    startDate={filter.dateFrom}
                                    startDateId="your_unique_start_date_id"
                                    endDate={filter.dateTo}
                                    endDateId="your_unique_end_date_id"
                                    onDatesChange={({ startDate, endDate }) => setFilter({ ...filter, dateFrom: startDate, dateTo: endDate })}
                                    focusedInput={focused}
                                    onFocusChange={(focusedInput) => setFocused(focusedInput)}
                                />
                            </div>
                        </div>
                        <div className="d-flex flex-row justify-content-end">
                            <button className='btn btn-danger d-flex' onClick={resetFilter}><FontAwesomeIcon icon={faEraser} className='me-2 my-auto'></FontAwesomeIcon>Reset</button>
                            <button className='btn btn-primary ms-2 d-flex' onClick={searchFilteredData}><FontAwesomeIcon icon={faSearch} className='me-2 my-auto'></FontAwesomeIcon>Cari</button>
                        </div>
                    </div>
                </div>
            )
        }

        return undefined;
    }

    return (
        <div>
            <LoadingPage loading={isLoading}/>
            <h1>Daftar Purchase Order</h1>
            <hr />
            <div className='p-1 bg-light shadow-lg'>
                <div className='d-flex flex-row justify-content-between'>
                    <p className='my-auto btn ms-auto fw-bold' onClick={changeFilterState}><FontAwesomeIcon icon={faFilter} className='mx-2'></FontAwesomeIcon>Filter</p>
                </div>
                {renderFilterForm()}
            </div>
            <div className="row">
                <div className='d-flex'>
                    <Link href="/purchaseOrder/create">
                        <a className='btn btn-secondary my-2 ms-auto'>
                            <FontAwesomeIcon icon={faPlus} className='me-2'></FontAwesomeIcon>Buat Purchase Order
                        </a>
                    </Link>
                </div>
            </div>
            <div className="d-flex flex-column">
            <PurchaseOrderGridData purchaseOrders={purchaseOrderList}></PurchaseOrderGridData>
            </div>
            <div className="d-flex flex-row">
                <Pagination dataLength={pagination.totalData} dataPerPage={pagination.itemPerPage} currentPage={pagination.currentPage} onPageChange={e => updatePaginate(pagination.itemPerPage, e)}></Pagination>
                <ItemPerPagePicker onChange={e => updatePaginate(e, 1)} />
            </div>
        </div>
    );
}

export default function PurchaseOrderPage() {
    return (
        <Authorize onlyLoggedIn={true}>
            <Layout title='Purchase Order'>
                <PurchaseOrder></PurchaseOrder>
            </Layout>
        </Authorize>
    )
};