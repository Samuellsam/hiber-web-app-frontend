import Layout from "../shared/Layout";
import React, { useEffect, useState } from "react";
import { DropdownModel, ProductClient } from "../../api/hiber-api";
import Select from "react-select";
import Swal from "sweetalert2";
import Joi from "joi";
import { RoleEnum } from "../../enums/enums";
import Authorize from "../shared/Authorize";
import LoadingPage from "../shared/components/LoadingPage";
import PriceInput, { ReturnType } from "../shared/components/PriceInput";
import BreadCrumbs from "../shared/components/BreadCrumbs";

interface ICreateProductForm {
    productName: string,
    productPrice: number,
    productBasePrice: number,
    supplier: string,
    productStock: number,
    productCategory: string,
    minimumStockLimit: number,
}

interface ICreateProductFormError {
    productName: {
        error: string
    },
    productPrice: {
        error: string
    },
    productBasePrice: {
        error: string
    },
    supplier: {
        error: string
    },
    productStock: {
        error: string
    },
    minimumStockLimit: {
        error: string
    },
    productCategory: {
        error: string
    },
}

export const CreateProductForm: React.FunctionComponent<{}> = () => {

    const [formValue, setFormValue] = useState<ICreateProductForm>({
        productBasePrice: 0,
        productCategory: '',
        productName: '',
        productPrice: 0,
        productStock: 0,
        supplier: '',
        minimumStockLimit: 1
    });

    const [photoForm, setPhotoForm] = useState<any>(null);

    const [formError, setFormError] = useState<ICreateProductFormError>({
        productBasePrice: {
            error: ''
        },
        productCategory: {
            error: ''
        },
        productName: {
            error: ''
        },
        productPrice: {
            error: ''
        },
        productStock: {
            error: ''
        },
        supplier: {
            error: ''
        },
        minimumStockLimit: {
            error: ''
        }
    })

    const [imagePreviewUrl, setImagePreviewUrl] = useState<any>("/assets/placeholder-image.png")

    const [isChanged, setIsChanged] = useState<boolean>(false);
    const [isSubmitting, setIsSubmitting] = useState<boolean>(false);
    const [client, setClient] = useState<ProductClient>();
    const [supplierList, setSupplierList] = useState<DropdownModel[]>([{
        label: '',
        value: ''
    }]);
    const [categoriesList, setCategoriesList] = useState<DropdownModel[]>([{
        label: '',
        value: ''
    }]);

    useEffect(() => {
        const initializeClient = async () => {
            const productClient = new ProductClient();
            initializeData(productClient);
            setClient(productClient);
        }
        initializeClient();
        return;
    }, [])

    const initializeData = async (productClient: ProductClient) => {
        if (!productClient) {
            return;
        }
        const data = await productClient.getFormData()

        if (data.supplierList) {
            setSupplierList(data.supplierList);
        }

        if (data.categoriesList) {
            setCategoriesList(data.categoriesList);
        }
    }

    const validateForm = (form, key?: string) => {
        const schema: {
            productBasePrice: Joi.SchemaLike,
            productCategory: Joi.SchemaLike,
            productName: Joi.SchemaLike,
            productPrice: Joi.SchemaLike,
            productStock: Joi.SchemaLike,
            supplier: Joi.SchemaLike
            minimumStockLimit: Joi.SchemaLike,
        } = {
            productBasePrice: '',
            productCategory: '',
            productName: '',
            productPrice: '',
            productStock: '',
            supplier: '',
            minimumStockLimit: ''
        }

        const errorMessages = {
            productBasePrice: '',
            productCategory: '',
            productName: '',
            productPrice: '',
            productStock: '',
            supplier: '',
            minimumStockLimit: ''
        }

        if (!key || key === 'productName') {
            schema['productName'] = Joi.string()
                .empty()
                .messages({
                    'string.empty': 'Nama Produk harus diisi'
                });
        }

        if (!key || key === 'productCategory') {
            schema['productCategory'] = Joi.string()
                .empty()
                .messages({
                    'string.empty': 'Kategori Produk harus diisi'
                });
        }

        if (!key || key === 'productBasePrice') {
            schema['productBasePrice'] = Joi.number()
                .min(1)
                .messages({
                    'number.min': 'Harga Modal Produk harus diisi'
                });
        }

        if (!key || key === 'productPrice') {
            schema['productPrice'] = Joi.number()
                .min(1)
                .messages({
                    'number.min': 'Harga Produk harus diisi',
                    'number.not': 'Harga Produk harus diisi'
                });
        }

        if (!key || key === 'productStock') {
            schema['productStock'] = Joi.number()
                .min(1)
                .messages({
                    'number.min': 'Stok Produk harus lebih dari 1'
                });
        }

        if (!key || key === 'supplier') {
            schema['supplier'] = Joi.string()
                .empty()
                .messages({
                    'string.empty': 'Supplier harus diisi'
                });
        }

        if (!key || key === 'minimumStockLimit') {
            schema['minimumStockLimit'] = Joi.number()
                .min(1)
                .messages({
                    'number.min': 'Minimum Stok Produk harus lebih dari 0'
                });
        }

        const validationResult = Joi.object(schema).validate(form, {
            abortEarly: false
        });

        const err = validationResult.error;

        if (!err) {
            return undefined;
        }

        for (const detail of err.details) {
            const key = detail.path[0]?.toString() ?? '';
            errorMessages[key] = detail.message;
        }

        return errorMessages;
    }



    const onFormChanged = (key: string, value: string) => {
        setIsChanged(true)
        const newFormValue: ICreateProductForm = formValue;
        newFormValue[key] = value;

        const createProductFormError: ICreateProductFormError = formError;

        const validationResult = validateForm(formValue, key);

        if (validationResult && validationResult[key]) {
            createProductFormError[key].error = validationResult[key];
        }
        else {
            createProductFormError[key].error = ''
        }
        setIsChanged(true);

        setFormValue({
            ...newFormValue
        });

        setFormError({
            ...createProductFormError
        })
    }



    const resetForm = () => {
        setFormError({
            productBasePrice: {
                error: ''
            },
            productCategory: {
                error: ''
            },
            productName: {
                error: ''
            },
            productPrice: {
                error: ''
            },
            productStock: {
                error: ''
            },
            supplier: {
                error: ''
            },
            minimumStockLimit: {
                error: ''
            }
        });
        setFormValue({
            productBasePrice: 0,
            productCategory: '',
            productName: '',
            productPrice: 0,
            productStock: 0,
            supplier: '',
            minimumStockLimit: 1,
        });
        setPhotoForm(null);
        setImagePreviewUrl("/assets/placeholder-image.png");
    }



    const onSubmitForm = async (e: React.ChangeEvent<HTMLFormElement>) => {
        e.preventDefault();
        const validationResult = validateForm(formValue);

        if (validationResult != undefined) {
            const createFormError: ICreateProductFormError = formError;

            for (const key in createFormError) {
                createFormError[key].dirty = true;

                if (validationResult && validationResult[key]) {
                    createFormError[key].error = validationResult[key];
                }
                else {
                    createFormError[key].error = '';
                }
            }

            setFormError({
                ...createFormError
            });
        }
        else {
            if (client) {

                try {
                    setIsSubmitting(true);
                    let response;
                    if (photoForm != null) {
                        response = await client.createProduct(formValue.productName, formValue.productStock, formValue.minimumStockLimit, formValue.productPrice, formValue.productBasePrice, parseInt(formValue.productCategory), formValue.supplier, photoForm.name, {
                            data: photoForm,
                            fileName: photoForm.name
                        });
                    } else {
                        response = await client.createProduct(formValue.productName, formValue.productStock, formValue.minimumStockLimit, formValue.productPrice, formValue.productBasePrice, parseInt(formValue.productCategory), formValue.supplier, null, null);
                    }

                    if (response) {
                        Swal.fire({
                            title: 'Berhasil membuat barang',
                            text: 'Barang baru berhasil ditambahkan',
                            icon: 'success'
                        });

                        setIsSubmitting(false);
                        resetForm();

                    }
                    else {
                        Swal.fire({
                            title: 'Gagal membuat barang',
                            text: 'Sebuah kesalahan telah terjadi. Silakan coba lagi atau hubungi administrator',
                            icon: 'error'
                        });
                        setIsSubmitting(false);
                    }
                } catch (error) {
                    Swal.fire({
                        title: 'Gagal membuat barang',
                        text: 'Sebuah kesalahan telah terjadi. Silakan coba lagi atau hubungi administrator',
                        icon: 'error'
                    });
                    setIsSubmitting(false);
                } finally {
                    setIsSubmitting(false);
                    setIsChanged(false);
                }
            }
        }
    }

    const onChangePicture = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.files && e.target.files[0]) {

            setIsChanged(true)
            let imageFile = e.target.files[0]

            const reader = new FileReader();
            reader.onload = x => {
                setPhotoForm(imageFile)
                setImagePreviewUrl(x.target?.result)
            }
            reader.readAsDataURL(imageFile);
        }

    }

    return (
        <div className='p-3'>
            <LoadingPage loading={isSubmitting} />
            <BreadCrumbs href="/product" isChanged={isChanged} text="Kembali ke Daftar produk" />
            <h3 className="mb-4">Buat Produk Baru</h3>
            <hr />
            <form onSubmit={onSubmitForm} >
                <fieldset disabled={isSubmitting}>
                    <div className='form-group mb-3'>
                        <div className="d-flex">
                            <div style={{ objectFit: 'contain' }}>
                                <img src={imagePreviewUrl} className="img-thumbnail" style={{ maxWidth: 400, maxHeight: 300 }} />
                            </div>
                            {photoForm !== null && <div className="btn btn-danger text-white align-self-center ms-4" onClick={() => {
                                setPhotoForm(null)
                                setImagePreviewUrl("/assets/placeholder-image.png")
                            }}>Hapus Foto</div>}

                        </div>

                        <label htmlFor="inputProductPhoto" className="form-label fw-bold">Foto Barang<span className='text-danger'>*</span></label>
                        <input accept="image/*" style={{ color: 'transparent' }} type="file" className="form-control hidden" id="inputProductPhoto" onChange={(e) => onChangePicture(e)}
                        />
                    </div>
                    <div className='form-group mb-3'>
                        <label htmlFor="inputNameProduct" className="form-label fw-bold">Nama Barang<span className='text-danger'>*</span></label>
                        <input type="text" className="form-control" id="inputNameProduct" value={formValue.productName} onChange={(e) => onFormChanged('productName', e.target.value)} />
                        <span className="text-danger small">{formError.productName.error}</span>
                    </div>

                    <div className='form-group mb-3 d-flex'>
                        <div className="col-md-4">
                            <label htmlFor="inputBasePrice" className="form-label fw-bold">Modal<span className='text-danger'>*</span></label>
                            <PriceInput value={formValue.productBasePrice} className="form-control" returnType={ReturnType.NUMBER} onChange={(e) => { onFormChanged('productBasePrice', e) }} />
                            <span className="text-danger small">{formError.productBasePrice.error}</span>

                        </div>
                        <div className="col-md-4"></div>
                        <div className="col-md-4">
                            <label htmlFor="inputProductPrice" className="form-label fw-bold">Harga Barang<span className='text-danger'>*</span></label>
                            <PriceInput value={formValue.productPrice} className="form-control" returnType={ReturnType.NUMBER} onChange={(e) => { onFormChanged('productPrice', e) }} />
                            <span className="text-danger small">{formError.productPrice.error}</span>
                        </div>
                    </div>

                    <div className='form-group mb-3 d-flex'>
                        <div className="col-md-4">
                            <label htmlFor="inputProductStock" className="form-label fw-bold">Jumlah Barang<span className='text-danger'>*</span></label>
                            <input type="number" className="form-control" id="inputProductStock" value={formValue.productStock} onChange={(e) => onFormChanged('productStock', e.target.value)} />
                            <span className="text-danger small">{formError.productStock.error}</span>
                            <br></br>
                            <label className="form-label fw-bold">Supplier<span className='text-danger'>*</span></label>
                            <Select
                                isSearchable={true}
                                options={supplierList}
                                value={{
                                    label: supplierList.find(Q => Q.value === formValue.supplier)?.label ?? "",
                                    value: formValue.supplier
                                }}
                                onChange={(e) => onFormChanged('supplier', e.value)}
                            />
                            <span className="text-danger small">{formError.supplier.error}</span>
                        </div>
                        <div className="col-md-4"></div>
                        <div className="col-md-4">
                            <label htmlFor="inputProductCategory" className="form-label fw-bold">Jenis Barang<span className='text-danger'>*</span></label>
                            <Select
                                isSearchable={true}
                                options={categoriesList}
                                value={{
                                    label: categoriesList.find(Q => Q.value === formValue.productCategory)?.label ?? "",
                                    value: formValue.productCategory
                                }}
                                onChange={(e) => onFormChanged('productCategory', e.value)}
                            />
                            <span className="text-danger small">{formError.productCategory.error}</span>
                            <br></br>
                            <label className="form-label fw-bold">Minimum Stok Limit<span className='text-danger'>*</span></label>
                            <input type="number" className="form-control" id="mnputMinimumStockLimitStock" value={formValue.minimumStockLimit} onChange={(e) => onFormChanged('minimumStockLimit', e.target.value)} />
                            <span className="text-danger small">{formError.minimumStockLimit.error}</span>
                        </div>
                    </div>

                    <div className="d-flex">
                        <input className='btn btn-primary ms-auto mb-4' type="submit" value={"Submit"}></input>
                    </div>

                </fieldset>
            </form>
        </div>
    )
}

export default function CreateProductPage() {
    return (
        <Authorize roleId={RoleEnum.ADMIN}>
            <Layout title="Create Product">
                <CreateProductForm></CreateProductForm>
            </Layout>
        </Authorize>
    )
}