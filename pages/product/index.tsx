import { faFilter, faPencilAlt, faTrash, faBarcode, faPlus, faSync, faEraser, faSearch, faInfoCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import moment from "moment";
import { useRouter } from "next/dist/client/router";
import JsBarcode from "jsbarcode";
import React, { useEffect, useState } from "react";
import Select from "react-select";
import Swal from "sweetalert2";
import { PaginationValue } from "../../interface/IPagination";
import { ItemPerPagePicker } from "../shared/components/Pagination/ItemPerPagePicker";
import { Pagination } from "../shared/components/Pagination/Pagination";
import { BarcodeFormModel, ProductClient, ProductViewModel } from "../../api/hiber-api";
import Layout from "../shared/Layout";
import { LIST_PAGE, PageSizeModel } from "../../constants/constants";
import { PageSizeEnum, RoleEnum } from "../../enums/enums";
import Authorize from "../shared/Authorize";
import LoadingPage from "../shared/components/LoadingPage";
import PriceFormat from "../shared/components/PriceFormat";
import { useCookies } from "react-cookie";

interface IFilterForm {
    name: string,
    categories: string
}
interface DropdownDataModel {
    label: string,
    value: string
}

interface PrintBarcodeModel {
    productId: string;
    productName: string;
    productBarcode: string;
    productQuantity: number;
}

interface PrintBarcodeForm {
    selectedDropdownProduct: DropdownDataModel;
    initialQuantity: number | string;
    initialBarcodeWidth: number | string;
    pageSizeEnum: number;
}

const PrintBarcodeModal: React.FunctionComponent<{
    listProduct: ProductViewModel[],
}> = (props) => {
    const [listPrintedBarcode, setListPrintedBarcode] = useState<PrintBarcodeModel[]>([]);
    const [listDropdownProduct, setListDropdownProduct] = useState<DropdownDataModel[]>([]);
    const [isAddButtonDisabled, setIsAddButtonDisabled] = useState<boolean>(true);
    const [listPageSize, setListPageSize] = useState<PageSizeModel[]>(LIST_PAGE);
    const [isPrintButtonDisabled, setIsPrintButtonDisabled] = useState<boolean>(true);
    const [form, setForm] = useState<PrintBarcodeForm>({
        initialQuantity: 1,
        initialBarcodeWidth: 5,
        selectedDropdownProduct: {
            label: '',
            value: ''
        },
        pageSizeEnum: PageSizeEnum.A4
    });

    useEffect(() => {
        validateForm();
    }, [form, listPrintedBarcode])

    useEffect(() => {
        initDropdown();
    }, [props.listProduct]);

    const validateForm = () => {
        if (form.initialBarcodeWidth.toString().trim() == '') {
            setIsPrintButtonDisabled(true);
        }
        else if (form.initialQuantity.toString().trim() == '') {
            setIsPrintButtonDisabled(true);
        }
        else if (listPrintedBarcode.length == 0) {
            setIsPrintButtonDisabled(true);
        }
        else {
            setIsPrintButtonDisabled(false);
        }
    }

    const initDropdown = () => {
        let dropdownList = props.listProduct.map(Q => {
            return {
                label: Q.productName,
                value: Q.productID
            } as DropdownDataModel
        });

        dropdownList.splice(0, 0, {
            label: 'Semua Barang',
            value: 'all'
        });

        setListDropdownProduct(dropdownList);
    }

    const resetForm = () => {
        initDropdown();
        setForm({
            initialQuantity: 1,
            initialBarcodeWidth: 5,
            selectedDropdownProduct: {
                label: '',
                value: ''
            },
            pageSizeEnum: form.pageSizeEnum
        });
        setIsAddButtonDisabled(false);
        setListPrintedBarcode([]);
    }

    const onRemoveClick = (productId: string) => {
        const newList = listPrintedBarcode.filter(Q => Q.productId != productId);
        setListPrintedBarcode(newList);
    }

    const renderListPrintedBarcode = () => {
        const rows = listPrintedBarcode.map(Q => {
            return (
                <div className='d-flex flex-row btn-group' key={Q.productId}>
                    <div className='d-flex flex-row p-2 btn mb-1 shadow-sm border border-2 flex-grow-1 w-100'>
                        <p className='me-2 my-auto d-flex flex-grow-1 fw-bold'>{Q.productName}</p>
                        <div className='d-flex justify-content-end'>
                            <p className='my-auto'>{Q.productQuantity + " kali"}</p>
                        </div>
                    </div>
                    <button className='btn btn-danger mb-1' onClick={() => onRemoveClick(Q.productId)}><FontAwesomeIcon icon={faTrash}></FontAwesomeIcon></button>
                </div>
            );
        });

        if (rows.length == 0) {
            return (
                <div className='w-100 h-100 d-flex'>
                    <p className='m-auto text-secondary'>No Data</p>
                </div>
            )
        }

        return rows;
    }

    const isProductAlreadyExist = (productId: string) => {
        const product = listPrintedBarcode.find(Q => Q.productId == productId);

        if (product == undefined) {
            return false;
        }

        return true;
    }

    const addProduct = () => {
        if (form.selectedDropdownProduct?.value == 'all') {
            props.listProduct.forEach(Q => {
                if (isProductAlreadyExist(Q.productID) == false) {
                    listPrintedBarcode.push({
                        productBarcode: Q.barcodeId ?? '',
                        productId: Q.productID,
                        productName: Q.productName ?? '',
                        productQuantity: form.initialQuantity == '' ? 1 : parseInt(form.initialQuantity.toString())
                    })
                }
            });

            setListPrintedBarcode([...listPrintedBarcode]);
        }
        else if (isProductAlreadyExist(form.selectedDropdownProduct?.value ?? '') == false) {
            const product = props.listProduct.find(Q => Q.productID == form.selectedDropdownProduct?.value);

            if (product !== undefined) {
                listPrintedBarcode.push({
                    productBarcode: product.barcodeId ?? '',
                    productId: product.productID,
                    productName: product.productName ?? '',
                    productQuantity: form.initialQuantity == '' ? 1 : parseInt(form.initialQuantity.toString())
                })

                setListPrintedBarcode([...listPrintedBarcode]);
            }
        }
        setIsAddButtonDisabled(true);
    }

    const onProductSelected = (selected) => {
        setIsAddButtonDisabled(false);
        if (isProductAlreadyExist(selected.value)) {
            setIsAddButtonDisabled(true);
        }
        else {
            setForm({ ...form, selectedDropdownProduct: selected });
        }
    }

    const base64ToArrayBuffer = (base64: string) => {
        var binary_string = window.atob(base64);
        var len = binary_string.length;
        var bytes = new Uint8Array(len);
        for (var i = 0; i < len; i++) {
            bytes[i] = binary_string.charCodeAt(i);
        }
        return bytes.buffer;
    }

    const generateBarcode = async () => {
        let listForm: BarcodeFormModel[] = [];
        setIsPrintButtonDisabled(true);

        listPrintedBarcode.forEach(Q => {
            JsBarcode('#tempBarcodeContainer', Q.productBarcode, {
                format: "CODE128",
                lineColor: '#000000',
                width: 2,
                height: 40,
                displayValue: true
            });
            var img = document.getElementById('tempBarcodeContainer');
            if (img) {
                var base64 = img['src'] as string;
                listForm.push({
                    productName: Q.productName,
                    quantity: Q.productQuantity,
                    productBase64BarcodeId: base64.replace('data:image/png;base64,', '')
                });
            }
        });

        const client = new ProductClient();
        try {
            const response = await client.printBarcode({
                listBarcodeModel: listForm,
                pageSizeEnum: form.pageSizeEnum,
                barcodeWidth: parseInt(form.initialBarcodeWidth as string)
            });
            var blobInstallment = new Blob([base64ToArrayBuffer(response)], { type: 'application/pdf' });
            var blobInstallmentURL = URL.createObjectURL(blobInstallment);
            window.open(blobInstallmentURL);

            Swal.fire({
                title: 'Berhasil print barcode',
                text: 'Print berhasil',
                icon: 'success'
            });

            resetForm();
        } catch (error) {
            Swal.fire({
                title: 'Gagal print barcode',
                text: 'Print gagal',
                icon: 'error'
            });
        } finally {
            setIsPrintButtonDisabled(false);
        }
    }

    const onPrintClick = () => {
        generateBarcode();
    }

    const onPageSelected = (pageEnum: number) => {
        const newListPage = listPageSize.map(element => {
            if (element.pageSizeEnum == pageEnum) {
                return {
                    ...element,
                    selected: true
                }
            }
            else {
                return {
                    ...element,
                    selected: false
                }
            }
        });

        const newForm = form;
        newForm.pageSizeEnum = pageEnum;

        setForm({ ...newForm });
        setListPageSize(newListPage);
    }

    const renderListPageSize = () => {
        const rows = listPageSize.map(Q => {
            return (
                <div key={Q.pageName} className={'btn text-center col-2 m-1 card shadow p-1' + (Q.selected ? ' bg-warning text-light' : '')} onClick={() => onPageSelected(Q.pageSizeEnum)}>
                    <h5>{Q.pageName}</h5>
                    <small>{Q.pageSize}</small>
                </div>
            )
        });

        if (rows.length == 0) {
            return (
                <div className='justify-content-center'>
                    <p>No Data</p>
                </div>
            )
        }

        return <div className='overflow-auto d-flex p-1 bg-dark'>{rows}</div>;
    }

    return (
        <div className="modal fade" id="printBarcodeStaticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex={-1} aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div className="modal-dialog modal-lg modal-dialog-scrollable d-flex">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="staticBackdropLabel">Cetak Barcode</h5>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div>
                        {renderListPageSize()}
                    </div>
                    {/* input product */}
                    <div className='d-flex shadow p-3'>
                        <div className='col-9'>
                            <Select
                                options={listDropdownProduct}
                                value={form.selectedDropdownProduct}
                                onChange={onProductSelected}
                            />
                        </div>
                        <input type="number" className='ms-2 form-control' placeholder={"Times"} value={form.initialQuantity} onChange={(e) => setForm({ ...form, initialQuantity: e.target.value.trim() == '' ? '' : parseInt(e.target.value) })} />
                        <button className='btn btn-info text-light mx-2' onClick={addProduct} disabled={isAddButtonDisabled}><FontAwesomeIcon icon={faPlus}></FontAwesomeIcon></button>
                        <button className='btn btn-warning text-light' onClick={resetForm}><FontAwesomeIcon icon={faSync}></FontAwesomeIcon></button>
                    </div>
                    <div className="modal-body d-flex flex-column">
                        <img id={"tempBarcodeContainer"} className='d-none' />
                        {/* list product */}
                        {renderListPrintedBarcode()}
                    </div>
                    <div className="modal-footer">
                        <div className='d-flex justify-content-between w-100'>
                            <div className="d-flex">
                                <p className='fw-bold my-auto col-5'>Lebar Barcode (cm)</p>
                                <input type="number" className='ms-2 form-control w-25' placeholder={"Width"} value={form.initialBarcodeWidth} onChange={(e) => setForm({ ...form, initialBarcodeWidth: e.target.value.trim() == '' ? '' : parseInt(e.target.value) })} />
                            </div>
                            <button type="button" className="btn btn-info text-light" disabled={isPrintButtonDisabled} onClick={onPrintClick}>Cetak</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

const ProductListComponent: React.FunctionComponent<{}> = () => {
    const Router = useRouter();
    const [filter, setFilter] = useState<IFilterForm>({
        categories: '',
        name: ''
    });
    const [productList, setProductList] = useState<ProductViewModel[]>();
    const [allProductList, setAllProductList] = useState<ProductViewModel[]>();
    const [client, setClient] = useState<ProductClient>();
    const [categoryList, setCategoryList] = useState<DropdownDataModel[]>([]);
    const [isFilterOpened, setIsFilterOpened] = useState<boolean>(false);
    const [pagination, setPagination] = useState<PaginationValue>({
        currentPage: 1,
        itemPerPage: 5,
        totalData: 0,
    });
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [cookies, setCookie, removeCookie] = useCookies(['roleId']);

    useEffect(() => {
        const InitClient = async () => {
            const productClient = new ProductClient();
            ResetData(productClient, pagination);
            setClient(productClient);
        }
        InitClient();
        return;
    }, [])


    const ResetData = async (productClient: ProductClient, paginate: PaginationValue) => {
        setIsLoading(true);
        try {
            const productData = await productClient.getAllProduct(filter.name, filter.categories, paginate.currentPage, paginate.itemPerPage);
            const allProductData = await productClient.getAllProduct(undefined, undefined, 0, 0);

            const categoryArray: DropdownDataModel[] = [];
            if (allProductData.products) {
                const distinctCategoryList = allProductData.products.map(item => item.categoryName)
                    .filter((value, index, self) => self.indexOf(value) === index);
                distinctCategoryList.forEach(product => {
                    categoryArray.push({
                        label: product ?? "",
                        value: product ?? ""
                    })
                });
            }

            paginate.totalData = productData.totalData;

            setCategoryList(categoryArray);
            setProductList(productData.products);
            setAllProductList(allProductData.products);
        } catch (error) {

        } finally {
            setIsLoading(false);
        }
    }

    const ResetFilter = async () => {
        setFilter({
            categories: '',
            name: ''
        })
        if (!client) {
            return;
        }
        setIsLoading(true);
        try {
            const productData = await client.getAllProduct(undefined, undefined, 1, pagination.itemPerPage);
            pagination.currentPage = 1;
            pagination.totalData = productData.totalData;
            setPagination(pagination);
            setProductList(productData.products);
        } catch (error) {

        } finally {
            setIsLoading(false);
        }
    }
    const onClickFilter = () => {
        if (!client) {
            return;
        }
        const paginate = pagination;
        paginate.currentPage = 1;
        ResetData(client, paginate);
    }
    const onClickResetFilter = () => {
        if (!client) {
            return;
        }
        ResetFilter();
    }
    const onDeleteProduct = async (id: string) => {
        const response = await Swal.fire({
            icon: 'error',
            title: "Hapus Barang?",
            html: '<span>Apakah anda yakin menghapus barang?</span> <br/> <br/><span class="text-danger">* Aksi ini tidak dapat dikembalikan</span>',
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak"
        })

        if (response.isConfirmed) {
            setIsLoading(true);
            try {
                const result = await client?.deleteProduct(id);

                if (result == true) {
                    Swal.fire({
                        title: "Sukses Mengapus Data",
                        text: "Data Sudah Disimpan",
                        allowOutsideClick: false,
                        confirmButtonText: 'OK'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            if (client)
                                ResetData(client, pagination);
                        }
                    })
                }
            } catch (error) {

            } finally {
                setIsLoading(false);
            }
        }
    }

    const renderFilterForm = () => {
        if (isFilterOpened) {
            return (
                <div>
                    <div className='px-3'>
                        <div className='d-flex flex-row justify-content-between my-1'>
                            <p className='w-50 my-auto'>Nama Produk</p>
                            <input type="text" value={filter.name} className='form-control w-50' placeholder='Search' onChange={e => setFilter({ categories: filter.categories, name: e.target.value })} />
                        </div>
                        <div className='d-flex flex-row justify-content-between mb-1'>
                            <p className='w-50 my-auto'>Kategori Produk</p>
                            <div className='w-50'>
                                <Select
                                    isSearchable={true}
                                    options={categoryList}
                                    value={{
                                        label: categoryList.find(Q => Q.value === filter.categories)?.label ?? "",
                                        value: filter.categories
                                    }}
                                    onChange={(e) => setFilter({ categories: e?.value ?? "", name: filter.name })}
                                />
                            </div>
                        </div>
                        <div className="d-flex flex-row justify-content-end">
                            <button className='btn btn-danger d-flex' onClick={() => onClickResetFilter()}><FontAwesomeIcon icon={faEraser} className='me-2 my-auto'></FontAwesomeIcon>Reset</button>
                            <button className='btn btn-primary ms-2 d-flex' onClick={() => onClickFilter()}><FontAwesomeIcon icon={faSearch} className='me-2 my-auto'></FontAwesomeIcon>Cari</button>
                        </div>
                    </div>
                </div>
            )
        }

        return undefined;

    }

    const updatePaginate = async (itemPerPage: number, currentPage: number) => {
        const paginate = pagination;
        paginate.itemPerPage = itemPerPage;
        paginate.currentPage = currentPage;
        setPagination(paginate);
        if (!client) {
            return;
        }
        ResetData(client, paginate);
    }

    const getDateWithFormat = (date: string) => {
        const dateWithFormat = moment(date).format("DD MMMM YYYY");
        return dateWithFormat;
    }

    return (
        <div>
            <LoadingPage loading={isLoading} />
            <h1>Daftar Produk</h1>
            <hr />
            {/* filter */}
            <div>
                <div className='p-1 bg-light shadow-lg mb-2'>
                    <div className='d-flex flex-row justify-content-between'>
                        <p className='my-auto btn ms-auto fw-bold' onClick={() => setIsFilterOpened(!isFilterOpened)}><FontAwesomeIcon icon={faFilter} className='mx-2'></FontAwesomeIcon>Filter</p>
                    </div>
                    {renderFilterForm()}
                </div>
            </div>

            {/* print barcode modal */}
            <PrintBarcodeModal listProduct={allProductList ?? []} />

            <div className="d-flex flex-row">
                <button className='btn btn-info text-light ms-auto mb-2 me-2' data-bs-toggle="modal" data-bs-target="#printBarcodeStaticBackdrop"><FontAwesomeIcon icon={faBarcode} className='me-2'></FontAwesomeIcon>Cetak Barcode</button>
                
                {cookies.roleId == RoleEnum.ADMIN ?
                    <button className='btn btn-secondary mb-2' onClick={() => { Router.push('/product/create') }}><FontAwesomeIcon icon={faPlus} className='me-2'></FontAwesomeIcon>Buat Produk Baru</button> : undefined}
            </div>

            {/* table data */}
            <table className="table table-hover table-striped">
                <thead >
                    <tr>
                        <th scope="col" className="text-center">Nama Produk</th>
                        <th scope="col" className="text-center">Kategori Barang</th>
                        <th scope="col" className="text-center">Harga</th>
                        <th scope="col" className="text-center">Stok</th>
                        
                        {cookies.roleId == RoleEnum.ADMIN ?
                            <th scope="col" className="text-center">Modal</th> : undefined}
    
                        <th scope="col" className="text-center">Terakhir Diubah</th>
                        <th scope="col" className="text-center"></th>
                    </tr>
                </thead>
                <tbody className="pointer" >
                    {productList?.map(Q =>
                        <tr key={Q.productID} >
                            <td className="pe-auto text-center">{Q.productName}</td>
                            <td className="text-center">{Q.categoryName}</td>
                            <td className="text-center"><PriceFormat value={Q.productPrice} /></td>
                            <td className="text-center">{`${Q.productStock} ${Q.unitName}`}</td>
                            {cookies.roleId == RoleEnum.ADMIN ?
                                <td className="text-center"><PriceFormat value={Q.productBasePrice} /></td> : undefined}
                            <td className="text-center">{getDateWithFormat(Q.lastUpdated)}</td>
                            <td className="text-center">
                                <button className='btn btn-primary me-2' onClick={() => Router.push(`/product/detail/${Q.productID}`)}>
                                    <div className="text-light">
                                        <FontAwesomeIcon icon={faInfoCircle} className='mx-1 text-light'></FontAwesomeIcon>Detail
                                    </div>
                                </button>
                                {cookies.roleId == RoleEnum.ADMIN ?
                                    <button className='btn btn-danger me-2' onClick={() => onDeleteProduct(Q.productID)}>
                                        <div className="text-light">
                                            <FontAwesomeIcon icon={faTrash} className='mx-1 text-light'></FontAwesomeIcon>Hapus
                                        </div>
                                    </button> : undefined}

                                {cookies.roleId == RoleEnum.ADMIN ?
                                    <button className='btn btn-warning me-2' onClick={() => Router.push(`/product/edit/${Q.productID}`)}>
                                        <div className="text-light">
                                            <FontAwesomeIcon icon={faPencilAlt} className='mx-1 text-light'></FontAwesomeIcon>Ubah
                                        </div>
                                    </button> : undefined}
                            </td>
                        </tr>)}

                </tbody>

            </table>

            <div className="d-flex flex-row">
                <Pagination dataLength={pagination.totalData} dataPerPage={pagination.itemPerPage} currentPage={pagination.currentPage} onPageChange={e => updatePaginate(pagination.itemPerPage, e)}></Pagination>
                <ItemPerPagePicker onChange={e => updatePaginate(e, 1)} />
            </div>

        </div>
    )
}

export default function ProductListPage() {
    return (
        <Authorize onlyLoggedIn={true}>
            <Layout
                title="Product List">
                <ProductListComponent></ProductListComponent>
            </Layout>
        </Authorize>
    )
}