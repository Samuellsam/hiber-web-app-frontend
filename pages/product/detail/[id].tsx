import { GetServerSideProps } from "next";
import React, { useEffect, useState } from "react";
import { useCookies } from "react-cookie";
import { ProductClient, ProductViewModel } from "../../../api/hiber-api";
import { RoleEnum } from "../../../enums/enums";
import Authorize from "../../shared/Authorize";
import BarcodeContainer from "../../shared/components/BarcodeContainer";
import BreadCrumbs from "../../shared/components/BreadCrumbs";
import { CopyClipboard } from "../../shared/components/CopyClipboard";
import LoadingPage from "../../shared/components/LoadingPage";
import PriceFormat from "../../shared/components/PriceFormat";
import Layout from "../../shared/Layout";

export const ProductDetail: React.FunctionComponent<{ id }> = (props) => {
    const [productDetail, setProductDetail] = useState<ProductViewModel>({
        lastUpdated: "",
        productBasePrice: 0,
        productCategoryId: 0,
        productID: "",
        productPrice: 0,
        productStock: 0,
        supplierId: "",
        barcodeId: "",
        categoryName: "",
        imageFile: null,
        imageName: '',
        imageSrc: '',
        productName: '',
        supplierName: '',
        unitName: '',
        minimumStockLimit: 1
    });
    const [client, setClient] = useState<ProductClient>();
    const [isLoading,setIsLoading] = useState<boolean>(true);
    const [cookies, setCookie, removeCookie] = useCookies(['roleId']);


    useEffect(() => {
        const initializeClient = async () => {
            const productClient = new ProductClient();
            initializeData(productClient);
            setClient(productClient);
        }
        initializeClient();
    }, [])

    const initializeData = async (productClient: ProductClient) => {
        setIsLoading(true)
        const data = await productClient.getProductDetail(props.id);
        const newProductDetail = data
        
        if (data.imageName === null) {
            newProductDetail.imageSrc = "/assets/placeholder-image.png";
        }
        setProductDetail(newProductDetail);
        setIsLoading(false);
    }

    return (
        <div className="container">
            <BreadCrumbs href="/product" isChanged={false} text="Kembali ke Daftar Produk"/>
            <LoadingPage loading={isLoading}/>
            <h1>Detail Barang</h1>
            <hr />
            <div className="card shadow mt-4" style={{borderRadius: 20}}>

                {productDetail &&
                    <div className="card-body px-5" >
                        <div className="row mb-3  mx-auto" style={{ width: 400, height: 300, objectFit: 'contain' }}>
                            <img className="card shadow py-2 " src={productDetail?.imageSrc} style={{ maxWidth: '100%', maxHeight: "100%" }} />
                        </div>

                        <div className="row">
                            <div className="col-md-6">
                                <div className="card-text mb-3 fw-bold">
                                    Nama Produk
                                </div>
                                <div className="card-text mb-3">
                                    {productDetail.productName}
                                </div>
                                <div className="card-text mb-3 fw-bold">
                                    Harga Produk
                                </div>
                                <div className="card-text mb-3">
                                    <PriceFormat value={productDetail.productPrice}/>
                                </div>
                                {cookies.roleId == RoleEnum.ADMIN ?
                                    <div className="card-text mb-3 fw-bold">
                                        Modal Produk
                                    </div> : undefined}
                                {cookies.roleId == RoleEnum.ADMIN ?
                                    <div className="card-text mb-3">
                                        <PriceFormat value={productDetail.productBasePrice}/>
                                    </div> : undefined}
                                <div className="card-text mb-3 fw-bold" >
                                    Supplier Produk
                                </div>
                                <div className="card-text mb-3">
                                    {productDetail.supplierName}
                                </div>
                            </div>
                            <div className="col-md-6 ">
                                <div className="card-text mb-3 fw-bold">
                                    Stok Produk
                                </div>
                                <div className="card-text mb-3">
                                    {productDetail.productStock} {productDetail.unitName}
                                </div>
                                <div className="card-text mb-3 fw-bold">
                                    Kategori Produk
                                </div>
                                <div className="card-text mb-3">
                                    {productDetail?.categoryName}
                                </div>
                                <div className="card-text mb-3 fw-bold">
                                    Minimum Stok Limit
                                </div>
                                <div className="card-text mb-3">
                                    {productDetail?.minimumStockLimit} {productDetail.unitName}
                                </div>
                                <div className="card-text mb-3 fw-bold">
                                    Barcode Produk
                                    <CopyClipboard text={productDetail.barcodeId !== undefined ? productDetail.barcodeId : ""} />
                                    <div className="mx-auto">
                                        <BarcodeContainer id={"barcode-code128"} type="CODE128" value={productDetail.barcodeId}></BarcodeContainer>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                }
            </div>
        </div>
    )
}

const ProductDetailPage: React.FunctionComponent<{ id: string }> = (props) => {
    return (
        <Authorize onlyLoggedIn={true}>
            <Layout title="Detail Product">
                <ProductDetail id={props.id}></ProductDetail>
            </Layout>
        </Authorize>
    )
}
export default ProductDetailPage;

export const getServerSideProps: GetServerSideProps<{ id: string }> = async (context) => {
    if (context.params) {
        const id = context.params['id'];

        if (typeof id === 'string') {
            return {
                props: {
                    id: id
                }
            }
        }
    }

    return {
        props: {
            id: ''
        }
    };
}