import { useRouter } from "next/dist/client/router"
import React, { useEffect, useState } from "react"
import { useCookies } from "react-cookie"
import { DashboardClient, TopNeedToBeDelivered, TopOutOfStockProduct, TopUnpaidTransaction, TotalFinancial } from "../../api/hiber-api"
import { RoleEnum } from "../../enums/enums"
import Authorize from "../shared/Authorize"
import DeliveryStatus from "../shared/components/delivery/deliveryStatus"
import LoadingPage from "../shared/components/LoadingPage"
import PriceFormat from "../shared/components/PriceFormat"
import Layout from "../shared/Layout"

const TopUnpaidDataGrid: React.FunctionComponent<{
    listUnpaid: TopUnpaidTransaction[]
}> = (props) => {
    const Router = useRouter();
    const renderRowData = () => {
        const rows = props.listUnpaid.map(Q => 
            <tr key={Q.transactionId} onClick={() => Router.push(`/transaction/${Q.transactionId}`)}>
                <td>
                    {Q.buyerName}
                </td>
                <td>
                    <PriceFormat value={Q.totalUnpaid}/>
                </td>
                <td>
                    {Q.deadlineDate}
                </td>
            </tr>);
        
        return rows.length == 0 ? (
            <tr>
                <td colSpan={5}>
                    No Data
                </td>
            </tr>
        ) : rows;
    }
    return (
        <div style={{maxHeight:300, overflow: 'auto'}}>
            <table className='table table-striped m-0 text-center table-hover' style={{maxHeight:400}} >
                <thead className='table-header'  style={{position: 'sticky',top: 0}}>
                    <tr>
                        <th>Pembeli</th>
                        <th>Tagihan</th>
                        <th>Batas Waktu</th>
                    </tr>
                </thead>
                <tbody className="pointer">
                    {renderRowData()}
                </tbody>
            </table>
        </div>
    )
}

const TopOutOfStockDataGrid: React.FunctionComponent<{
    listOutOfStock: TopOutOfStockProduct[]
}> = (props) => {
    const Router = useRouter();
    const renderRowData = () => {
        const rows = props.listOutOfStock.map(Q => 
            <tr key={Q.productId} >
                <td>
                    {Q.productName}
                </td>
                <td>
                    <PriceFormat value={Q.productPrice}/>
                </td>
                <td>
                    {Q.productStock}
                </td>
                <td>
                    <PriceFormat value={Q.productModal}/>
                </td>
                <td>
                    {Q.productLastUpdate}
                </td>
            </tr>);
        
        return rows.length == 0 ? (
            <tr>
                <td colSpan={5}>
                    No Data
                </td>
            </tr>
        ) : rows;
    }
    return (
        <div style={{maxHeight:300, overflow: 'auto'}}>
            <table className='table table-striped m-0 text-center' style={{maxHeight:400}}>
                <thead className='table-header' style={{position: 'sticky',top: 0}}>
                    <tr>
                        <th>Nama Produk</th>
                        <th>Harga</th>
                        <th>Stock</th>
                        <th>Modal</th>
                        <th>Last Update</th>
                    </tr>
                </thead>
                <tbody>
                    {renderRowData()}
                </tbody>
            </table>
        </div>
    )
}

const TopNeedDeliveredDataGrid: React.FunctionComponent<{
    listNeedDeliver: TopNeedToBeDelivered[]
}> = (props) => {
    const Router = useRouter();
    const renderRowData = () => {
        const rows = props.listNeedDeliver.map(Q => 
            <tr key={Q.deliveryId} className='pointer' onClick={() => Router.push(`/delivery/${Q.deliveryId}`)}>
                <td>
                    {Q.buyerName}
                </td>
                <td>
                    <DeliveryStatus type={Q.deliveryStatusId}></DeliveryStatus>
                </td>
                <td>
                    {Q.driverName}
                </td>
                <td>
                    {Q.orderDate}
                </td>
            </tr>);
        
        return rows.length == 0 ? (
            <tr>
                <td colSpan={4}>
                    No Data
                </td>
            </tr>
        ) : rows;
    }
    return (
        <div style={{maxHeight:400, overflow: 'auto'}}>
            <table className='table table-striped m-0 text-center table-hover' style={{maxHeight:500}} >
                <thead className='table-header' style={{position: 'sticky',top: 0}} >
                    <tr >
                        <th>Nama Pembeli</th>
                        <th>Status</th>
                        <th>Kurir</th>
                        <th>Tanggal Pembelian</th>
                    </tr>
                </thead>
                <tbody>
                    {renderRowData()}
                </tbody>
            </table>
        </div>
    )
}

const TotalFinancialGrid: React.FunctionComponent<{
    totalFinancial: TotalFinancial
}> = (props) => {
    return (
        <div>
            <div className='table m-0'>
                <div className='flex-row'>
                    <div className='bg-danger text-light d-flex flex-grow-1 flex-column card m-1'>
                        <p className='fw-bold text-start'>Pendapatan hari ini</p>
                        <h5 className='text-end'><PriceFormat value={props.totalFinancial.totalRevenueToday}/></h5>
                    </div>
                    <div className='bg-primary text-light d-flex flex-grow-1 flex-column card m-1'>
                        <p className='fw-bold text-start'>Pendapatan bulan ini</p>
                        <h5 className='text-end'><PriceFormat value={props.totalFinancial.totalRevenueThisMonth}/></h5>
                    </div>
                    <div className='bg-success text-light d-flex flex-grow-1 flex-column card m-1'>
                        <p className='fw-bold text-start'>Total pendapatan</p>
                        <h5 className='text-end'><PriceFormat value={props.totalFinancial.totalRevenue}/></h5>
                    </div>
                </div>
            </div>
        </div>
    )
}

const Dashboard: React.FunctionComponent<{}> = (props) => {
    const [listUnpaid, setListUnpaid] = useState<TopUnpaidTransaction[]>([]);
    const [listNeedDeliver, setListNeedDeliver] = useState<TopNeedToBeDelivered[]>([]);
    const [listOutOfStock, setListOutOfStock] = useState<TopOutOfStockProduct[]>([]);
    const [totalFinancial, setTotalFinancial] = useState<TotalFinancial>({
        totalRevenue : 0,
        totalRevenueThisMonth : 0,
        totalRevenueToday : 0
    });
    const [cookies, setCookie, removeCookie] = useCookies(['username', 'userId', 'roleId', 'roleName']);
    const [isLoading, setIsLoading] = useState<boolean>(true);
    
    useEffect(() => {
        fetchData();
    }, []);

    const fetchData = async () => {
        const client = new DashboardClient();
        setIsLoading(true);
        try {
            if(cookies.roleId == RoleEnum.ADMIN) {
                const response = await client.getAdminDashboard();
                setListNeedDeliver(response.needToBeDeliveredDeliveries as TopNeedToBeDelivered[]);
                setListOutOfStock(response.outOfStockProducts as TopOutOfStockProduct[]);
                setListUnpaid(response.unpaidTransactions as TopUnpaidTransaction[]);
                setTotalFinancial(response.totalFinancial as TotalFinancial);
            }
            else if(cookies.roleId == RoleEnum.EMPLOYEE) {
                const response = await client.getEmployeeDashboard(cookies.userId);
                setListNeedDeliver(response.needToBeDeliveredDeliveries as TopNeedToBeDelivered[]);
                setListOutOfStock(response.outOfStockProducts as TopOutOfStockProduct[]);
            }
        } catch (error) {
            
        } finally {
            setIsLoading(false);
        }
    }

    return (
        <div>
            <LoadingPage loading={isLoading}/>
            <div className='card shadow p-3'>
                <h3>Selamat Datang, {cookies.username}</h3>
            </div>
            <br />
            {cookies.roleId == RoleEnum.ADMIN ? 
                <div>
                    <div className='d-flex flex-row justify-content-between'>
                        <div className='col-7 card shadow p-3 d-flex flex-grow-1 me-4'>
                            <h4>Pesanan Yang Perlu Diantar</h4>
                            <TopNeedDeliveredDataGrid listNeedDeliver={listNeedDeliver}></TopNeedDeliveredDataGrid>
                        </div>
                        <div className='col-4 card shadow p-3 justify-content-center'>
                            <h4>Total Omset</h4>
                            <TotalFinancialGrid totalFinancial={totalFinancial}></TotalFinancialGrid>
                        </div>
                    </div>
                    <br />
                    <div className="d-flex flex-row justify-content-between">
                        <div className='col-4 card shadow p-3 me-4'>
                            <h4>Daftar Utang</h4>
                            <TopUnpaidDataGrid listUnpaid={listUnpaid}></TopUnpaidDataGrid>
                        </div>
                        <div className='col-7 card shadow p-3 d-flex flex-grow-1'>
                            <h4>Stok Barang Yang Akan Habis</h4>
                            <TopOutOfStockDataGrid listOutOfStock={listOutOfStock}></TopOutOfStockDataGrid>
                        </div>
                    </div>
                </div> : 
                <div>
                    <div className='card shadow p-3 d-flex flex-grow-1'>
                        <h4>Pesanan Yang Perlu Diantar</h4>
                        <TopNeedDeliveredDataGrid listNeedDeliver={listNeedDeliver}></TopNeedDeliveredDataGrid>
                    </div>
                    <br />
                    <div className='card shadow p-3 d-flex flex-grow-1'>
                        <h4>Stok Barang Yang Akan Habis</h4>
                        <TopOutOfStockDataGrid listOutOfStock={listOutOfStock}></TopOutOfStockDataGrid>
                    </div>
                </div>}
        </div>
    )
}

export default function DashboardPage() {
    return (
        <Authorize onlyLoggedIn={true}>
            <Layout title={"Dashboard"}>
                <Dashboard></Dashboard>
            </Layout>
        </Authorize>
    )
}