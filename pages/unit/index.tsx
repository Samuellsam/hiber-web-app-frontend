import { faEraser, faFilter, faPencilAlt, faPlus, faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import {  CategoryViewModel, UnitClient, UnitCreateUpdateModel, UnitPaginationModel, UnitViewModel } from "../../api/hiber-api";
import { RoleEnum } from "../../enums/enums";
import Authorize from "../shared/Authorize";
import Layout from "../shared/Layout";
import Select from 'react-select';
import { DropdownModel } from "../transaction";
import Swal from "sweetalert2";
import { PaginationValue } from "../../interface/IPagination";
import { ItemPerPagePicker } from "../shared/components/Pagination/ItemPerPagePicker";
import { Pagination } from "../shared/components/Pagination/Pagination";
import LoadingPage from "../shared/components/LoadingPage";

interface CreateUnitForm {
    unitName: string;
    category: DropdownModel;
}

const UnitCreateModal: React.FunctionComponent<{
    onCreate: () => void,
    categories: DropdownModel[],
    setShowLoading: (isShow: boolean) => void,
}> = (props) => {
    const [form, setForm] = useState<CreateUnitForm>({
        category: {
            label: '',
            value: -1
        },
        unitName: ''
    });
    const [isDisable, setIsDisable] = useState<boolean>(true);
    useEffect(() => {
        const newForm = form;
        newForm.category = {
            label: '',
            value: -1
        };
    }, [props.categories]);
    useEffect(() => {
        if(form.category.value != -1 && form.unitName.trim() != '') {
            setIsDisable(false);
        }
        else {
            setIsDisable(true);
        }
    }, [form]);
    const onUnitNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        const newForm = form;
        newForm.unitName = value;

        setForm({ ...newForm });
    }
    const onUnitCreate = async () => {
        props.setShowLoading(true);
        const client = new UnitClient();
        try {
            const isSuccess = await client.createUnit({
                categoryId: form.category.value ?? -1,
                unitName: form.unitName
            });
            if (isSuccess == true) {
                Swal.fire({
                    title: 'Sukses menambah satuan ukur',
                    text: 'Satuan ukur berhasil ditambahkan',
                    icon: 'success'
                });
                const newForm = form;
                newForm.category = {
                    label: '',
                    value: -1
                };
                newForm.unitName = ''
                setForm({...newForm})
                props.onCreate();
            }
            else {
                Swal.fire({
                    title: 'Gagal menambah data satuan ukur',
                    text: 'Data satuan ukur sudah memiliki unit',
                    icon: 'error'
                });
            }
        } catch (error) {
            Swal.fire({
                title: 'Gagal menambah data satuan ukur',
                text: 'Gagal menambah data satuan ukur',
                icon: 'error'
            });
        } finally {
            props.setShowLoading(false);
        }
    }
    const onCategoryChange = (selected) => {
        const newForm = form;
        newForm.category = selected;

        setForm({ ...newForm });
    }
    return (
        <div className="modal fade" id="staticCreateUnitBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex={-1} aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="staticBackdropLabel">Buat Satuan Ukur Baru</h5>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body">
                        <label className="form-label">Nama Satuan Ukur</label>
                        <input type="text" className='form-control mb-3' value={form.unitName} onChange={onUnitNameChange} />
                        <label className="form-label">Kategori</label>
                        <Select options={props.categories} onChange={onCategoryChange} value={form.category} autoFocus />
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                        <button type="button" className="btn btn-primary" data-bs-dismiss="modal" onClick={onUnitCreate} disabled={isDisable}>Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

const UnitUpdateModal: React.FunctionComponent<{
    categories: DropdownModel[],
    unitId: number,
    onUpdate: () => void,
    setShowLoading: (isShow: boolean) => void,
    changer: boolean
}> = (props) => {
    const [form, setForm] = useState<CreateUnitForm>({
        category: {
            label: '',
            value: -1
        },
        unitName: ''
    });
    useEffect(() => {
        fetchUpdateData();
    }, [props.changer]);

    const fetchUpdateData = async () => {
        const client = new UnitClient();
        try {
            const response = await client.getUnit(props.unitId);

            const newForm = form;
            newForm.unitName = response.unitName ?? '';
            newForm.category = {
                label: response.categoryName ?? '',
                value: response.categoryId
            }

            setForm({ ...newForm })
        } catch (error) {

        }
    }

    const onUnitNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        const newForm = form;
        newForm.unitName = value;

        setForm({ ...newForm });
    }
    const onUnitUpdate = async () => {
        props.setShowLoading(true);
        const client = new UnitClient();
        try {
            const response = await client.updateUnit({
                categoryId: form.category.value ?? -1,
                unitName: form.unitName,
            }, props.unitId);
            if (response.isSuccess == true) {
                Swal.fire({
                    title: 'Sukses mengubah satuan ukur',
                    text: 'Satuan ukur berhasil diubah',
                    icon: 'success'
                });
                setForm({
                    category: props.categories[0] ?? {
                        label: '',
                        value: -1
                    },
                    unitName: ''
                })
                props.onUpdate();
            }
            else {
                Swal.fire({
                    title: response.errorMessage,
                    text: 'Gagal mengubah satuan ukur',
                    icon: 'error'
                });
            }
        } catch (error) {
            Swal.fire({
                title: 'Gagal mengubah satuan ukur',
                text: 'Gagal mengubah satuan ukur',
                icon: 'error'
            });
        } finally {
            props.setShowLoading(false);
        }
    }
    const onCategoryChange = (selected) => {
        const newForm = form;
        newForm.category = selected;

        setForm({ ...newForm });
    }
    return (
        <div className="modal fade" id="staticUpdateUnitBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex={-1} aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="staticBackdropLabel">Ubah Satuan Ukur</h5>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body">
                        <label className="form-label">Nama Satuan Ukur</label>
                        <input type="text" className='form-control mb-3' value={form.unitName} onChange={onUnitNameChange} />
                        <label className="form-label">Kategori</label>
                        <Select options={props.categories} onChange={onCategoryChange} value={form.category} autoFocus />
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                        <button type="button" className="btn btn-primary" data-bs-dismiss="modal" onClick={onUnitUpdate}>Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

const UnitGridData: React.FunctionComponent<{
    units: UnitViewModel[],
    onUnitSelected: (unitId: number) => void,
}> = (props) => {
    const onEditClick = (unitId: number) => {
        props.onUnitSelected(unitId);
    }
    const renderRowData = () => {
        const rows = props.units.map(Q => {
            return (
                <tr key={Q.unitId}>
                    <td>
                        <p>{Q.unitId}</p>
                    </td>
                    <td>
                        <p>{Q.unitName}</p>
                    </td>
                    <td>
                        <p>{Q.categoryName}</p>
                    </td>
                    <td>

                        <button className='btn btn-warning' type="button" data-bs-toggle="modal" data-bs-target="#staticUpdateUnitBackdrop" onClick={() => onEditClick(Q.unitId)}>
                            <div className="text-light">
                                <FontAwesomeIcon icon={faPencilAlt} className='mx-1 text-light'></FontAwesomeIcon>Ubah
                            </div>
                        </button>
                    </td>
                </tr>
            )
        });
        return rows;
    }

    return (
        <div >
            <table className='table table-striped m-0 text-center table-hover'>
                <thead className='table-header'>
                    <tr>
                        <th>ID Satuan Ukur</th>
                        <th>Nama Satuan Ukur</th>
                        <th>Kategori</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {renderRowData()}
                </tbody>
            </table>
        </div>
    );
}

interface UnitFilterForm {
    unitName: string | undefined;
    categoryName: string | undefined;
}

const Unit: React.FunctionComponent<{}> = (props) => {
    const [listUnit, setListUnit] = useState<UnitViewModel[]>([]);
    const [unitId, setUnitIdModal] = useState<number>(1);
    const [changer, setChanger] = useState<boolean>(true);
    const [listCategory, setListCategory] = useState<DropdownModel[]>([]);
    const [filterForm, setFilterForm] = useState<UnitFilterForm>({
        categoryName: undefined,
        unitName: undefined
    });
    const [isFilterOpened, setIsFilterOpened] = useState<boolean>(false);
    const [pagination, setPagination] = useState<PaginationValue>({
        currentPage: 1,
        itemPerPage: 5,
        totalData: 0,
    });
    const [isLoading, setIsLoading] = useState<boolean>(true);

    useEffect(() => {
        getData();
        fetchData(pagination, filterForm);
    }, []);

    const getData = async () => {
        setIsLoading(true);
        const clientUnit = new UnitClient();
        try {
            const categories = await clientUnit.getRemainingCategory() as CategoryViewModel[];
            const categoriesDropdown = categories.map(Q => {
                return {
                    label: Q.categoryName as string,
                    value: Q.categoryId
                } as DropdownModel
            });
            setListCategory(categoriesDropdown);
        } catch (error) {

        } finally {
            setIsLoading(false);
        }
    }

    const fetchData = async (pagination: PaginationValue, filter: UnitFilterForm) => {
        setIsLoading(true);
        getData();
        const client = new UnitClient();
        try {
            const data: UnitPaginationModel = await client.getAllUnit(filter.unitName, filter.categoryName, pagination.currentPage, pagination.itemPerPage);
            setPagination({ ...pagination, totalData: data.totalData })
            setListUnit(data.units as UnitViewModel[]);
        } catch (error) {

        } finally {
            setIsLoading(false);
        }
    }

    const resetData = () => {
        getData();
        const newPagination = {
            ...pagination,
            currentPage: 1
        };
        const newFilterForm = {
            categoryName: '',
            unitName: ''
        };

        setFilterForm({ ...newFilterForm });
        setPagination({ ...newPagination });

        try {
            fetchData(newPagination, newFilterForm);
        } catch (error) {

        }
    }

    const onUnitSelected = (unitId: number) => {
        setUnitIdModal(unitId);
        setChanger(!changer);
    }

    const updatePaginate = async (itemPerPage: number, currentPage: number) => {
        const paginate = pagination;
        paginate.itemPerPage = itemPerPage;
        paginate.currentPage = currentPage;
        setPagination(paginate);

        fetchData(paginate, filterForm);
    }

    const searchFilteredData = () => {
        const newPagination = {
            ...pagination,
            currentPage: 1
        };

        setPagination({ ...newPagination });
        try {
            fetchData(newPagination, filterForm);
        } catch (error) {

        }
    }

    const renderFilterForm = () => {
        if (isFilterOpened) {
            return (
                <div>
                    <div className='px-3'>
                        <div className='d-flex flex-row justify-content-between my-1'>
                            <p className='w-50 my-auto'>Nama Unit</p>
                            <input type="text" value={filterForm.unitName} className='form-control w-50' placeholder='Search' onChange={e => setFilterForm({ ...filterForm, unitName: e.target.value })} />
                        </div>
                        <div className='d-flex flex-row justify-content-between my-1'>
                            <p className='w-50 my-auto'>Nama Kategori</p>
                            <input type="text" value={filterForm.categoryName} className='form-control w-50' placeholder='Search' onChange={e => setFilterForm({ ...filterForm, categoryName: e.target.value })} />
                        </div>
                        <div className="d-flex flex-row justify-content-end">
                            <button className='btn btn-danger d-flex' onClick={resetData}><FontAwesomeIcon icon={faEraser} className='me-2 my-auto'></FontAwesomeIcon>Reset</button>
                            <button className='btn btn-primary ms-2 d-flex' onClick={searchFilteredData}><FontAwesomeIcon icon={faSearch} className='me-2 my-auto'></FontAwesomeIcon>Cari</button>
                        </div>
                    </div>
                </div>
            )
        }

        return undefined;
    }

    return (
        <div className='d-flex flex-column'>
            <LoadingPage loading={isLoading} />
            <h1>Daftar Satuan Ukur</h1>
            <hr />
            <div className='p-1 bg-light shadow-lg'>
                <div className='d-flex flex-row justify-content-between'>
                    <p className='my-auto btn ms-auto fw-bold' onClick={() => setIsFilterOpened(!isFilterOpened)}><FontAwesomeIcon icon={faFilter} className='mx-2'></FontAwesomeIcon>Filter</p>
                </div>
                {renderFilterForm()}
            </div>

            <UnitCreateModal onCreate={() => fetchData(pagination, filterForm)} categories={listCategory} setShowLoading={isShow => setIsLoading(isShow)} />
            <UnitUpdateModal onUpdate={() => fetchData(pagination, filterForm)} categories={listCategory} unitId={unitId} changer={changer} setShowLoading={isShow => setIsLoading(isShow)} />
            <button type="button" className='btn btn-secondary my-2 ms-auto' data-bs-toggle="modal" data-bs-target="#staticCreateUnitBackdrop"><FontAwesomeIcon icon={faPlus} className='me-2'></FontAwesomeIcon>Tambah Satuan Ukur</button>
            <UnitGridData units={listUnit} onUnitSelected={onUnitSelected}></UnitGridData>

            <div className="d-flex flex-row">
                <Pagination dataLength={pagination.totalData} dataPerPage={pagination.itemPerPage} currentPage={pagination.currentPage} onPageChange={e => updatePaginate(pagination.itemPerPage, e)}></Pagination>
                <ItemPerPagePicker onChange={e => updatePaginate(e, 1)} />
            </div>
        </div>
    );
}

export default function UnitPage() {
    return (
        <Authorize roleId={RoleEnum.ADMIN}>
            <Layout title={"Unit"}>
                <Unit></Unit>
            </Layout>
        </Authorize>
    );
}