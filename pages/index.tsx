import { useRouter } from "next/dist/client/router";
import { useEffect } from "react";
import { useCookies } from "react-cookie";

const Index: React.FunctionComponent<{}> = (props) => {
  const router = useRouter();
  const [cookies, setCookie] = useCookies(['username', 'userId', 'roleId', 'roleName']);

  useEffect(() => {
    checkIsCookieExist();
  });

  const checkIsCookieExist = () => {
    if(cookies.username != null && cookies.userId != null && cookies.roleName != null && cookies.roleId != null) {
        router.push('/dashboard');
    }
    else {
        router.push('/login');
    }
  }
  return (
    <div></div>
  )
}

export default Index;