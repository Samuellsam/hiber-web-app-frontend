import { faEraser, faFilter, faPencilAlt, faPlus, faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";
import { CategoryClient, CategoryCreateUpdateResponseModel, CategoryPaginationModel, CategoryViewModel } from "../../api/hiber-api";
import { RoleEnum } from "../../enums/enums";
import { PaginationValue } from "../../interface/IPagination";
import Authorize from "../shared/Authorize";
import LoadingPage from "../shared/components/LoadingPage";
import { ItemPerPagePicker } from "../shared/components/Pagination/ItemPerPagePicker";
import { Pagination } from "../shared/components/Pagination/Pagination";
import Layout from "../shared/Layout";

const CategoryCreateModal: React.FunctionComponent<{
    onCreate: () => void
    setShowLoading: (isShow: boolean) => void,
}> = (props) => {
    const [categoryName, setCategoryName] = useState<string>('');
    const [isDisabled, setIsDisabled] = useState<boolean>(true);
    useEffect(() => {
        if(categoryName.trim() != '') {
            setIsDisabled(false);
        }
        else {
            setIsDisabled(true);
        }
    }, [categoryName])

    const onCategoryCreate = async () => {
        const client = new CategoryClient();
        props.setShowLoading(true);
        try {
            const isSuccess = await client.createCategory({
                categoryName: categoryName
            });
            if (isSuccess == true) {
                Swal.fire({
                    title: 'Sukses menambah kategori',
                    text: 'Kategori berhasil ditambahkan',
                    icon: 'success'
                });
                setCategoryName('')
                props.onCreate();
            }
            else {
                Swal.fire({
                    title: 'Gagal menambah data kategori',
                    text: 'Gagal menambah data kategori',
                    icon: 'error'
                });
            }
        } catch (error) {
            Swal.fire({
                title: 'Gagal menambah data kategori',
                text: 'Gagal menambah data kategori',
                icon: 'error'
            });
        } finally {
            props.setShowLoading(false);
        }
    }
    
    return (
        <div className="modal fade" id="staticCreateCategoryBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex={-1} aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="staticBackdropLabel">Buat Kategori Baru</h5>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body">
                        <label htmlFor="inputCategoryName" className="form-label">Nama Kategori</label>
                        <input type="text" id="inputCategoryName" className="form-control" value={categoryName} onChange={e => setCategoryName(e.target.value)} />
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                        <button type="button" className="btn btn-primary" data-bs-dismiss="modal" onClick={onCategoryCreate} disabled={isDisabled}>Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

const CategoryUpdateModal: React.FunctionComponent<{
    categoryId: number,
    onUpdate: () => void,
    setShowLoading: (isShow: boolean) => void,
    changer: boolean
}> = (props) => {
    const [categoryName, setCategoryName] = useState<string>('');
    useEffect(() => {
        fetchData();
    }, [props.changer]);

    const fetchData = async () => {
        props.setShowLoading(true);
        const client = new CategoryClient();
        try {
            const category = await client.getCategory(props.categoryId);
            setCategoryName(category.categoryName ?? '');
        } catch (error) {

        } finally {
            props.setShowLoading(false);
        }
    }

    const onCategoryUpdate = async () => {
        const client = new CategoryClient();
        props.setShowLoading(true);
        try {
            const response: CategoryCreateUpdateResponseModel = await client.updateCategory({
                categoryName: categoryName
            }, props.categoryId);
            if (response.isSuccess == true) {
                Swal.fire({
                    title: 'Sukses mengubah kategori',
                    text: 'Kategori berhasil diubah',
                    icon: 'success'
                });
                props.onUpdate();
            }
            else if (response.isSuccess == false) {
                Swal.fire({
                    title: response.errorMessage,
                    text: 'Gagal mengubah data kateogri',
                    icon: 'error'
                });
            }
            else {
                Swal.fire({
                    title: 'Gagal mengedit data kategori',
                    text: 'Gagal mengubah data kategori',
                    icon: 'error'
                });
            }
        } catch (error) {
            Swal.fire({
                title: 'Gagal mengedit data kategori',
                text: 'Gagal mengubah data kategori',
                icon: 'error'
            });
        } finally {
            props.setShowLoading(false);
        }
    }

    return (
        <div className="modal fade" id="staticUpdateCategoryBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex={-1} aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="staticBackdropLabel">Ubah Kategori</h5>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body">
                        <label htmlFor="inputCategoryName" className="form-label">Nama Kategori</label>
                        <input type="text" id="inputCategoryName" className="form-control" value={categoryName} onChange={(e) => setCategoryName(e.target.value)} />
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                        <button type="button" className="btn btn-primary" data-bs-dismiss="modal" onClick={onCategoryUpdate}>Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

const CategoryGridData: React.FunctionComponent<{
    categories: CategoryViewModel[],
    onCategorySelected: (categoryId: number) => void
}> = (props) => {
    const onEditClick = (categoryId: number) => {
        props.onCategorySelected(categoryId);
    }
    const renderRowData = () => {
        return props.categories.map(Q => {
            return (
                <tr key={Q.categoryId}>
                    <td>
                        <p>{Q.categoryId}</p>
                    </td>
                    <td>
                        <p>{Q.categoryName}</p>
                    </td>
                    <td>
                        <button className='btn btn-warning' type="button" data-bs-toggle="modal" data-bs-target="#staticUpdateCategoryBackdrop" onClick={() => onEditClick(Q.categoryId)}>
                            <div className="text-light">
                                <FontAwesomeIcon icon={faPencilAlt} className='mx-1 text-light'></FontAwesomeIcon>Ubah
                            </div>
                        </button>
                    </td>
                </tr>
            )
        });
    }

    return (
        <div >
            <table className='table table-striped m-0 text-center table-hover'>
                <thead className='table-header'>
                    <tr>
                        <th>ID Kategori</th>
                        <th>Nama Kategori</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {renderRowData()}
                </tbody>
            </table>
        </div>
    );
}

const Category: React.FunctionComponent<{}> = (props) => {
    const [listCategory, setListCategory] = useState<CategoryViewModel[]>([]);
    const [categoryId, setCategoryId] = useState<number>(-1);
    const [changer, setChanger] = useState<boolean>(true);
    const [filterCategoryName, setFilterCategoryName] = useState<string | undefined>(undefined);
    const [isFilterOpened, setIsFilterOpened] = useState<boolean>(false);
    const [pagination, setPagination] = useState<PaginationValue>({
        currentPage: 1,
        itemPerPage: 5,
        totalData: 0,
    });
    const [isLoading, setIsLoading] = useState<boolean>(true);

    useEffect(() => {
        fetchData(pagination, filterCategoryName);
    }, []);

    const fetchData = async (pagination: PaginationValue, categoryName: string | undefined) => {
        setIsLoading(true);
        const client = new CategoryClient();
        try {
            const data: CategoryPaginationModel = await client.getAllCategory(categoryName, pagination.currentPage, pagination.itemPerPage);
            setPagination({ ...pagination, totalData: data.totalData })
            setListCategory(data.categories as CategoryViewModel[]);
        } catch (error) {

        } finally {
            setIsLoading(false);
        }
    }

    const resetData = () => {
        const newPagination = {
            ...pagination,
            currentPage: 1
        };

        setFilterCategoryName('');
        setPagination({ ...newPagination });

        try {
            fetchData(newPagination, '');
        } catch (error) {

        }
    }

    const onCategorySelected = (categoryId: number) => {
        setCategoryId(categoryId);
        setChanger(!changer);
    }

    const updatePaginate = async (itemPerPage: number, currentPage: number) => {
        const paginate = pagination;
        paginate.itemPerPage = itemPerPage;
        paginate.currentPage = currentPage;
        setPagination(paginate);

        fetchData(paginate, filterCategoryName);
    }

    const searchFilteredData = () => {
        const newPagination = {
            ...pagination,
            currentPage: 1
        };

        setPagination({ ...newPagination });
        try {
            fetchData(newPagination, filterCategoryName);
        } catch (error) {

        }
    }

    const renderFilterForm = () => {
        if (isFilterOpened) {
            return (
                <div>
                    <div className='px-3'>
                        <div className='d-flex flex-row justify-content-between my-1'>
                            <p className='w-50 my-auto'>Nama Kategori</p>
                            <input type="text" value={filterCategoryName} className='form-control w-50' placeholder='Search' onChange={e => setFilterCategoryName(e.target.value)} />
                        </div>
                        <div className="d-flex flex-row justify-content-end">
                            <button className='btn btn-danger d-flex' onClick={resetData}><FontAwesomeIcon icon={faEraser} className='me-2 my-auto'></FontAwesomeIcon>Reset</button>
                            <button className='btn btn-primary ms-2 d-flex' onClick={searchFilteredData}><FontAwesomeIcon icon={faSearch} className='me-2 my-auto'></FontAwesomeIcon>Cari</button>
                        </div>
                    </div>
                </div>
            )
        }

        return undefined;
    }

    return (
        <div className='d-flex flex-column'>
            <LoadingPage loading={isLoading} />
            <h1>Daftar Kategori Barang</h1>
            <hr />
            <div className='p-1 bg-light shadow-lg'>
                <div className='d-flex flex-row justify-content-between'>
                    <p className='my-auto btn ms-auto fw-bold' onClick={() => setIsFilterOpened(!isFilterOpened)}><FontAwesomeIcon icon={faFilter} className='mx-2'></FontAwesomeIcon>Filter</p>
                </div>
                {renderFilterForm()}
            </div>

            <CategoryCreateModal onCreate={() => fetchData(pagination, filterCategoryName)} setShowLoading={isShow => setIsLoading(isShow)} />
            <CategoryUpdateModal categoryId={categoryId} onUpdate={() => fetchData(pagination, filterCategoryName)} changer={changer} setShowLoading={isShow => setIsLoading(isShow)} />

            <button type="button" className='btn btn-secondary my-2 ms-auto' data-bs-toggle="modal" data-bs-target="#staticCreateCategoryBackdrop"><FontAwesomeIcon icon={faPlus} className='me-2'></FontAwesomeIcon>Tambah Kategori</button>

            <CategoryGridData categories={listCategory} onCategorySelected={onCategorySelected}></CategoryGridData>

            <div className="d-flex flex-row">
                <Pagination dataLength={pagination.totalData} dataPerPage={pagination.itemPerPage} currentPage={pagination.currentPage} onPageChange={e => updatePaginate(pagination.itemPerPage, e)}></Pagination>
                <ItemPerPagePicker onChange={e => updatePaginate(e, 1)} />
            </div>
        </div>
    );
}

export default function CategoryPage() {
    return (
        <Authorize roleId={RoleEnum.ADMIN}>
            <Layout title={"Category"}>
                <Category></Category>
            </Layout>
        </Authorize>
    );
}