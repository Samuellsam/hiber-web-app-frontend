import { GetServerSideProps } from "next";
import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";
import { DeliveryClient, DeliveryDetailViewModel, DeliveryProductDetail } from "../../api/hiber-api";
import { DeliveryStatusEnum, RoleEnum } from "../../enums/enums";
import Authorize from "../shared/Authorize";
import BreadCrumbs from "../shared/components/BreadCrumbs";
import { CopyClipboard } from "../shared/components/CopyClipboard";
import DeliveryStatus from "../shared/components/delivery/deliveryStatus";
import LoadingPage from "../shared/components/LoadingPage";
import PriceFormat from "../shared/components/PriceFormat";
import Layout from "../shared/Layout";

const DeliveryDetail: React.FunctionComponent<{deliveryId: string}> = (props) => {
    const [detail, setDetail] = useState<DeliveryDetailViewModel>({
        transactionId: '',
        statusId: 1,
        address: '',
        buyerName: '',
        driverName: '',
        listBoughtProduct: [],
        totalPrice: 0
    });

    const [isSubmitting, SetIsSubmitting] = useState<boolean>(false);
    const [isLoading, setIsLoading] = useState<boolean>(true);

    useEffect(() => {
        fetchData();
    }, []);

    const fetchData = async () => {
        const client = new DeliveryClient();
        setIsLoading(true);
        try {
            const data: DeliveryDetailViewModel = await client.getDeliveryByDeliveryId(props.deliveryId);
            setDetail(data);
        } catch (error) {
            
        } finally {
            setIsLoading(false);
        }
    }

    const renderBoughtProducts = () => {
        const list: DeliveryProductDetail[] = detail.listBoughtProduct as DeliveryProductDetail[];
        return list.map(Q => 
            <tr key={Q.productName}>
                <td>
                    <p>{Q.productName}</p>
                </td>
                <td>
                    <p>{Q.productQuantity+" "+Q.productUnitName}</p>
                </td>
                <td>
                    <p><PriceFormat value={Q.productPrice}/></p>
                </td>
            </tr>);
    }

    const changeDeliveryStatus = async () => {
        SetIsSubmitting(true);
        const client = new DeliveryClient();
        try {
            const newDeliveryStatus = await client.changeDeliveryStatus(props.deliveryId);
            setDetail({
                ...detail,
                statusId: newDeliveryStatus
            });
            Swal.fire({
                title: 'Berhasil mengganti status delivery',
                text: 'Status delivery telah diubah',
                icon: 'success'
            });
        } catch (error) {
            Swal.fire({
                title: 'Gagal mengganti status delivery',
                text: 'Status delivery gagal diubah',
                icon: 'error'
            });
        } finally {
            SetIsSubmitting(false);
        }
    }

    const getDeliveryButton = () => {
        let status = '';
        if(detail.statusId == DeliveryStatusEnum.MEMUAT_BARANG) {
            status = 'Kirim Pesanan';
        }
        else if(detail.statusId == DeliveryStatusEnum.MENUJU_CUSTOMER) {
            status = 'Selesaikan Pengantaran';
        }
        else if(detail.statusId == DeliveryStatusEnum.BARANG_SAMPAI) {
            return undefined;
        }

        return <button className='btn btn-primary my-auto' onClick={changeDeliveryStatus} disabled={isSubmitting}>{status}</button>;
    }
    
    return (
        <div>
            <LoadingPage loading={isLoading}/>
            <BreadCrumbs href="/delivery" isChanged={false} text="Kembali ke list pengiriman"/>
            <h3 className="mb-4">Detail Pengiriman</h3>
            <hr />
            <div className='row'>
                <div className='col-3'>
                    <p>ID Delivery</p>
                </div>
                <div className='col-9'>
                    <p className='d-flex'>{props.deliveryId}<CopyClipboard text={props.deliveryId}/></p>
                </div>
            </div>
            <div className='row'>
                <div className='col-3'>
                    <p>ID Transaksi</p>
                </div>
                <div className='col-9'>
                    <p className='d-flex'>{detail.transactionId}<CopyClipboard text={detail.transactionId}/></p>
                </div>
            </div>
            <div className='row'>
                <div className='col-3'>
                    <p>Nama Pembeli</p>
                </div>
                <div className='col-9'>
                    <p>{detail.buyerName}</p>
                </div>
            </div>
            <div className='row'>
                <div className='col-3'>
                    <p>Nama Pengirim</p>
                </div>
                <div className='col-9'>
                    <p>{detail.driverName}</p>
                </div>
            </div>
            <div className='row'>
                <div className='col-3'>
                    <p>Alamat</p>
                </div>
                <div className='col-9'>
                    <p>{detail.address}</p>
                </div>
            </div>
            <div className='row'>
                <div className='col-3'>
                    <p>Status</p>
                </div>
                <div className='col-9'>
                    <DeliveryStatus type={detail.statusId}></DeliveryStatus>
                </div>
            </div>
            <br />
            <div>
                <p className='text-center fw-bold'>Barang yang dibeli</p>
                <table className='table table table-striped m-0 text-center table-hover'>
                    <thead className='table-header'>
                        <tr>
                            <th>Nama Produk</th>
                            <th>Kuantitas</th>
                            <th>Harga</th>
                        </tr>
                    </thead>
                    <tbody>
                        {renderBoughtProducts()}
                    </tbody>
                </table>
            </div>
            <br />
            <br />
            <div className='d-flex'>
                <p className='ms-auto my-auto'>Total Harga: <PriceFormat value={detail.totalPrice}/></p>
                <div className="col-1"></div>
                {getDeliveryButton()}
            </div>
        </div>
    );
}

const DeliveryDetailPage: React.FunctionComponent<{deliveryId: string}> = (props) => {
    return (
        <Authorize roleId={RoleEnum.ADMIN}>
            <Layout title='Delivery Detail'>
                    <DeliveryDetail deliveryId={props.deliveryId}></DeliveryDetail>
            </Layout>
        </Authorize>
    );
}

export const getServerSideProps: GetServerSideProps<{ deliveryId: string }> = async (context) => {
    if (context.params) {
        const deliveryId = context.params['deliveryId'];

        if (typeof deliveryId === 'string') {
            return {
                props: {
                    deliveryId: deliveryId
                }
            }
        }
    }

    return {
        props: {
            deliveryId: ''
        }
    };
}

export default DeliveryDetailPage;