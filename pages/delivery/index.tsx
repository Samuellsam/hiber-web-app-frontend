import Link from "next/link";
import React, { useEffect, useState } from "react";
import { DeliveryClient, DeliveryFilterDataModel, DeliveryPaginationModel, DeliveryViewModel, DropdownIntModel } from "../../api/hiber-api";
import { RoleEnum } from "../../enums/enums";
import { PaginationValue } from "../../interface/IPagination";
import Authorize from "../shared/Authorize";
import DeliveryStatus from "../shared/components/delivery/deliveryStatus";
import { ItemPerPagePicker } from "../shared/components/Pagination/ItemPerPagePicker";
import { Pagination } from "../shared/components/Pagination/Pagination";
import Layout from "../shared/Layout";
import { DropdownModel, FilterModel } from "../transaction";
import Select from 'react-select';
import { faEraser, faFilter, faInfoCircle, faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { CopyClipboard } from "../shared/components/CopyClipboard";
import LoadingPage from "../shared/components/LoadingPage";
import moment from "moment";

const DeliveryGridData: React.FunctionComponent<{
    deliveries: DeliveryViewModel[]
}> = (props) => {

    const getDateWithFormat = (date: string) =>{
        const dateWithFormat = moment(date).format("DD MMMM YYYY");
        return dateWithFormat;
    }

    const renderRowData = () => {
        return props.deliveries.map(Q => 
            <tr key={Q.deliveryId}>
                <td >
                    <p>{Q.deliveryId}
                    <CopyClipboard text={Q.deliveryId}/>
                    </p>
                </td>
                <td>
                    <p>{Q.buyerName}</p>
                </td>
                <td>
                    <p>{Q.driverName}</p>
                </td>
                <td>
                    <p>{getDateWithFormat(Q.createdAt)}</p>
                </td>
                <td>
                    <DeliveryStatus type={Q.deliveryStatusId}/>
                </td>
                <td>
                    <Link href={'/delivery/' + Q.deliveryId}>
                        <a className='btn btn-primary'> <FontAwesomeIcon icon={faInfoCircle} className='mx-1 text-light'></FontAwesomeIcon>Detail</a>
                    </Link>
                </td>
            </tr>);
    }

    return (
        <div>
            <table className='table table-striped m-0 text-center table-hover'>
                <thead className='table-header'>
                    <tr>
                        <th>ID Pengiriman</th>
                        <th>Nama Pembeli</th>
                        <th>Nama Pengantar</th>
                        <th>Tanggal Pemesanan</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {renderRowData()}
                </tbody>
            </table>
        </div>
    );
}

interface DeliveryFilterModel {
    deliveryId: string;
    buyerName: string;
    driver: DropdownModel;
    deliveryStatus: DropdownIntModel;
}

const Delivery: React.FunctionComponent<{}> = (props) => {
    const [listDelivery, setListDelivery] = useState<DeliveryViewModel[]>([]);
    const [listDriver, setListDriver] = useState<DropdownModel[]>([]);
    const [listDeliveryStatus, setListDeliveryStatus] = useState<DropdownIntModel[]>([]);
    const [isFilterShowed, setIsFilterShowed] = useState<boolean>(false);
    const [filter, setFilter] = useState<DeliveryFilterModel>({
        buyerName: '',
        deliveryId: '',
        deliveryStatus: {
            label: '',
            value: undefined
        },
        driver: {
            label: '',
            value: undefined
        }
    });
    const [pagination, setPagination] = useState<PaginationValue>({
        currentPage: 1,
        itemPerPage: 5,
        totalData: 0,
    });
    const [isLoading, setIsLoading] = useState<boolean>(true);

    useEffect(() => {
        getFilterData();
        fetchData(pagination, filter);
    }, []);

    const fetchData = async (paginate: PaginationValue, filterData: DeliveryFilterModel) => {
        const client = new DeliveryClient();
        setIsLoading(true);
        
        try {
            const data: DeliveryPaginationModel = await client.getAllDelivery(filterData.deliveryId, filterData.buyerName, filterData.driver.value?.toString(), filterData.deliveryStatus.value as number, paginate.currentPage, paginate.itemPerPage);
            setPagination({...paginate, totalData: data.totalData});

            if (data.deliveries)
                setListDelivery([...data.deliveries]);
        } catch (error) {
            
        } finally {
            setIsLoading(false);
        }
    }

    const searchFilteredData = () => {
        const newPagination = {
            ...pagination,
            currentPage: 1
        };

        setPagination({...newPagination});
        try {
            fetchData(newPagination, filter);
        } catch (error) {
            
        }
    }

    const getFilterData = async () => {
        const client = new DeliveryClient();
        setIsLoading(true);
        try {
            const data: DeliveryFilterDataModel = await client.getFilterData();

            setListDriver(data.drivers as DropdownModel[]);
            setListDeliveryStatus(data.deliveryStatuses as DropdownIntModel[]);
        } catch (error) {

        } finally {
            setIsLoading(false);
        }
    }

    const resetData = () => {
        const newFilter = {
            buyerName: '',
            deliveryId: '',
            deliveryStatus: {
                label: '',
                value: undefined
            },
            driver: {
                label: '',
                value: undefined
            }
        }
        const newPagination = {
            ...pagination,
            currentPage: 1
        };

        setFilter({...newFilter});
        setPagination({...newPagination});

        try {
            fetchData(newPagination, newFilter);
        } catch (error) {
            
        }
    }

    const updatePaginate = async (itemPerPage: number, currentPage: number) => {
        const paginate = pagination;
        paginate.itemPerPage = itemPerPage;
        paginate.currentPage = currentPage;
        setPagination({...paginate});
        fetchData(paginate, filter);
    }

    const onBuyerNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        const newFilter = filter;
        newFilter.buyerName = value;
        setFilter({
            ...newFilter
        });
    }

    const onDeliveryIdChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        const newFilter = filter;
        newFilter.deliveryId = value;
        setFilter({
            ...newFilter
        });
    }

    const renderFilterForm = () => {
        if(isFilterShowed) {
            return (
                <div>
                    <div className='px-3'>
                        <div className='d-flex flex-row justify-content-between my-1'>
                            <p className='w-50 my-auto'>Nama Pembeli</p>
                            <input type="text" value={filter.buyerName} className='form-control w-50' placeholder='Search Buyer' onChange={onBuyerNameChange}/>
                        </div>
                        <div className='d-flex flex-row justify-content-between my-1'>
                            <p className='w-50 my-auto'>ID Pengiriman</p>
                            <input type="text" value={filter.deliveryId} className='form-control w-50' placeholder='Search DeliveryId' onChange={onDeliveryIdChange}/>
                        </div>
                        <div className='d-flex flex-row justify-content-between mb-1'>
                            <p className='w-50 my-auto'>Driver</p>
                            <div className='w-50'>
                                <Select options={listDriver} onChange={(selected) => setFilter({...filter, driver: selected})} value={filter.driver}/>
                            </div>
                        </div>
                        <div className='d-flex flex-row justify-content-between mb-1'>
                            <p className='w-50 my-auto'>Delivery Status</p>
                            <div className='w-50'>
                                <Select options={listDeliveryStatus} onChange={(selected) => setFilter({...filter, deliveryStatus: selected})} value={filter.deliveryStatus}/>
                            </div>
                        </div>
                        <div className="d-flex flex-row justify-content-end">
                            <button className='btn btn-danger d-flex' onClick={resetData}><FontAwesomeIcon icon={faEraser} className='me-2 my-auto'></FontAwesomeIcon>Reset</button>
                            <button className='btn btn-primary ms-2 d-flex' onClick={searchFilteredData}><FontAwesomeIcon icon={faSearch} className='me-2 my-auto'></FontAwesomeIcon>Cari</button>
                        </div>
                    </div>
                </div>
            )
        }

        return undefined;
    }

    return (
        <div>
            <LoadingPage loading={isLoading}/>
            <h1>Daftar Pengiriman</h1>
            <hr />
            <div className='p-1 bg-light shadow-lg'>
                <div className='d-flex flex-row justify-content-between'>
                    <p className='my-auto btn ms-auto fw-bold' onClick={() => setIsFilterShowed(!isFilterShowed)}><FontAwesomeIcon icon={faFilter} className='mx-2'></FontAwesomeIcon>Filter</p>
                </div>
                {renderFilterForm()}
            </div>
            <br />
            <DeliveryGridData deliveries={listDelivery}></DeliveryGridData>
            <div className="d-flex flex-row">
                <Pagination dataLength={pagination.totalData} dataPerPage={pagination.itemPerPage} currentPage={pagination.currentPage} onPageChange={e => updatePaginate(pagination.itemPerPage, e)}></Pagination>
                <ItemPerPagePicker onChange={e => updatePaginate(e, 1)} />
            </div>
        </div>
    );
}

export default function DeliveryPage() {
    return (
        <Authorize roleId={RoleEnum.ADMIN}>
            <Layout title='Delivery'>
                    <Delivery></Delivery>
            </Layout>
        </Authorize>
    );
}