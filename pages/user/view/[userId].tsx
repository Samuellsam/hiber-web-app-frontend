import { faEdit, faPassport, faPen } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Joi from "joi";
import { GetServerSideProps } from "next";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import { useCookies } from "react-cookie";
import Swal from "sweetalert2";
import { DeliveryClient, DeliveryDetailViewModel, DeliveryProductDetail, UserClient, UserUpdateModel, UserViewModel } from "../../../api/hiber-api";
import Authorize from "../../shared/Authorize";
import BreadCrumbs from "../../shared/components/BreadCrumbs";
import DeliveryStatus from "../../shared/components/delivery/deliveryStatus";
import LoadingPage from "../../shared/components/LoadingPage";
import Layout from "../../shared/Layout";
import { PasswordInput } from "../create";

interface UserDetailViewModel {
    username: string;
    name: string;
    email: string;
    isAdmin: boolean;
}

const UserDetail: React.FunctionComponent<{userId: string}> = (props) => {
    const [userView, setUserView] = useState<UserDetailViewModel>(
        {
            username: '',
            name: '',
            email: '',
            isAdmin: false
        }
    )
    const [cookies, setCookie] = useCookies(['username', 'userId', 'roleId', 'roleName']);
    const [isLoading, setIsLoading] = useState<boolean>(true);

    useEffect(() => {
        fetchUserData();
    }, []);

    const fetchUserData = async () => {
        setIsLoading(true);
        const client = new UserClient();
        try {
            const data = await client.getUser(props.userId);
            setUserView({
                email: data.email as string,
                isAdmin: data.roleId == 1,
                name: data.name as string,
                username: data.username as string
            });
        } catch (error) {
            
        } finally {
            setIsLoading(false);
        }
    }

    return (
        <div>
            <LoadingPage loading={isLoading}/>
            <BreadCrumbs href="/dashboard" isChanged={false} text="Kembali ke dashboard"/>
            <h3>Detail akun</h3>
            <hr />
            <div className='form-group mb-3'>
                <label className="form-label fw-bold">Name</label>
                <p>{userView.name}</p>
            </div>
            <div className='form-group mb-3'>
                <label className="form-label fw-bold">Username</label>
                <p>{userView.username}</p>
            </div>
            <div className='form-group mb-3'>
                <label className="form-label fw-bold">Email</label>
                <p>{userView.email}</p>
            </div>
            <div className='form-group mb-3'>
                <label className="form-label fw-bold">Role</label>
                <p>{userView.isAdmin ? 'Admin' : 'Karyawan'}</p>
            </div>
            {props.userId == cookies.userId ? 
                <Link href={'/user/edit/changePassword/' + props.userId}>
                    <a className='btn btn-warning'><FontAwesomeIcon icon={faEdit} className='me-2 my-auto'></FontAwesomeIcon>Ubah Password</a>
                </Link> : undefined}
        </div>
    );
}

const UserDetailPage: React.FunctionComponent<{userId: string}> = (props) => {
    return (
        <Authorize onlyLoggedIn={true}>
            <Layout title='User Detail'>
                <UserDetail userId={props.userId}></UserDetail>
            </Layout>
        </Authorize>
    );
}

export const getServerSideProps: GetServerSideProps<{ userId: string }> = async (context) => {
    if (context.params) {
        const userId = context.params['userId'];

        if (typeof userId === 'string') {
            return {
                props: {
                    userId: userId
                }
            }
        }
    }

    return {
        props: {
            userId: ''
        }
    };
}

export default UserDetailPage;