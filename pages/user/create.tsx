import React, { useState } from "react";
import Layout from "../shared/Layout";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import Joi from "joi";
import { UserClient } from "../../api/hiber-api";
import Swal from "sweetalert2";
import Authorize from "../shared/Authorize";
import { RoleEnum } from "../../enums/enums";
import LoadingPage from "../shared/components/LoadingPage";
import BreadCrumbs from "../shared/components/BreadCrumbs";

export const PasswordInput: React.FunctionComponent<{
    className ?: string,
    placeholder ?: string,
    value: string,
    onChange: (value: string) => void
}> = (props) => {
    const [isOpened, setIsOpened] = useState<boolean>(false);

    const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        props.onChange(e.target.value);
    }
    
    const getInputType = () => {
        if(isOpened) {
            return 'text'
        }

        return 'password'
    }

    const getIcon = () => {
        if(isOpened) {
            return faEyeSlash
        }

        return faEye
    }
    
    const openPassword = () => {
        setIsOpened(!isOpened)
    }
    
    return (
        <div className='input-group'>
            <input type={getInputType()} value={props.value} className={"form-control "+props.className} onChange={onChange} placeholder={props.placeholder}/>
            <span className="input-group-text btn border border-secondary bg-light" onClick={openPassword}>
                <FontAwesomeIcon icon={getIcon()}></FontAwesomeIcon>
            </span>
        </div>
    );
}

interface RegisterForm {
    username: string;
    name: string;
    email: string;
    password: string;
    confirmPassword: string;
    isAdmin: boolean;
}

interface RegisterFormError{
    username: {
        error: string,
    }
    name: {
        error: string,
    }
    email: {
        error: string,
    }
    password: {
        error: string,
    }
    confirmPassword: {
        error: string,
    }
}

const Account: React.FunctionComponent<{}> = (props) => {
    const [registerForm, setRegisterForm] = useState<RegisterForm>(
        {
            username: '',
            name: '',
            email: '',
            password: '',
            confirmPassword: '',
            isAdmin: false
        }
    )

    const [registerFormError, setRegisterFormError] = useState<RegisterFormError>({
        username: {
            error: '',
        },
        name: {
            error: '',
        },
        email: {
            error: '',
        },
        password: {
            error: '',
        },
        confirmPassword: {
            error: '',
        }
    });

    const [isSubmitting, setIsSubmitting] = useState<boolean>(false);
    const [isChanged, setIsChanged] = useState<boolean>(false);

    const onFormChange = (key: string, value: string) => {
        setIsChanged(true);

        const newFormValue: RegisterForm = registerForm;
        newFormValue[key] = value

        const newRegisterFormError: RegisterFormError = registerFormError;

        const formToValidate = {
            username: registerForm.username,
            name: registerForm.name,
            email: registerForm.email,
            password: registerForm.password,
            confirmPassword: registerForm.confirmPassword,
        }

        const validationResult = validateForm(formToValidate, key);
        
        if (validationResult && validationResult[key]) {
            newRegisterFormError[key].error = validationResult[key];
        }
        else{
            newRegisterFormError[key].error = ''
        }
        setRegisterForm({
            ...newFormValue
        });
        setRegisterFormError({
            ...newRegisterFormError
        });
    }

    const onRoleChange = (value: boolean) => {
        setIsChanged(true);

        const newFormValue: RegisterForm = registerForm;
        newFormValue.isAdmin = value;
        setRegisterForm({
            ...newFormValue
        });
    }

    const validateForm = (form, key?: string) => {
        const schema: {
            username: Joi.SchemaLike,
            name: Joi.SchemaLike,
            email: Joi.SchemaLike,
            password: Joi.SchemaLike,
            confirmPassword: Joi.SchemaLike,
        } = {
            username: '',
            name: '',
            email: '',
            password: '',
            confirmPassword: '',
        }

        const errorMessages = {
            username: '',
            name: '',
            email: '',
            password: '',
            confirmPassword: '',
        };

        if (!key || key === 'username') {
            schema['username'] = Joi.string()
                .empty()
                .messages({
                    'string.empty': 'Username harus diisi',
                });
        }
        
        if (!key || key === 'name') {
            schema['name'] = Joi.string()
                .empty()
                .messages({
                    'string.empty': 'Nama harus diisi',
                });
        }
        
        if (!key || key === 'email') {
            schema['email'] = Joi.string()
                .empty()
                .email({ tlds: { allow: false } })
                .messages({
                    'string.empty': 'Email harus diisi',
                    'string.email': 'Email tidak valid',
                });
        }
        
        if (!key || key === 'password') {
            schema['password'] = Joi.string()
                .empty()
                .messages({
                    'string.empty': 'Password harus diisi',
                });
        }
        
        if (!key || key === 'confirmPassword') {
            schema['confirmPassword'] = Joi.string().empty().valid(Joi.ref('password'))
                .messages({
                    'string.empty': 'Confirm password harus diisi',
                    'any.only': 'Confirm password harus sama dengan password'
                })
        }

        const validationResult = Joi.object(schema).validate(form, {
            abortEarly: false
        });

        const err = validationResult.error;
        
        if (!err){
            return undefined;
        }

        for (const detail of err.details){
            const key = detail.path[0]?.toString() ?? '';
            errorMessages[key] = detail.message;
        }

        return errorMessages;
    }

    const resetForm = () => {
        setRegisterForm({
            username: '',
            name: '',
            email: '',
            password: '',
            confirmPassword: '',
            isAdmin: false
        });
        setRegisterFormError({
            username: {
                error: '',
            },
            name: {
                error: '',
            },
            email: {
                error: '',
            },
            password: {
                error: '',
            },
            confirmPassword: {
                error: '',
            }
        });
    }

    const onFormSubmit = async (e: React.ChangeEvent<HTMLFormElement>) => {
        e.preventDefault();
        const formToValidate = {
            username: registerForm.username,
            name: registerForm.name,
            email: registerForm.email,
            password: registerForm.password,
            confirmPassword: registerForm.confirmPassword,
        }
        const validationResult = validateForm(formToValidate);
        
        if(validationResult != undefined) {
            const newRegisterFormError: RegisterFormError = registerFormError;

            for (const key in newRegisterFormError) {
                newRegisterFormError[key].dirty = true;
    
                if (validationResult && validationResult[key]) {
                    newRegisterFormError[key].error = validationResult[key];
                }
                else{
                    newRegisterFormError[key].error = '';
                }
            }
            
            setRegisterFormError({
                ...newRegisterFormError
            });
        }
        else {
            setIsSubmitting(true);
            try {
                const client = new UserClient();
                const success = await client.createUser(registerForm);
                if(success) {
                    Swal.fire({
                        title: 'Berhasil membuat user',
                        text: 'User baru berhasil ditambahkan',
                        icon: 'success'
                    });

                    resetForm();
                }
                else {
                    Swal.fire({
                        title: 'Gagal membuat user',
                        text: 'Sebuah kesalahan telah terjadi. Silakan coba lagi atau hubungi administrator',
                        icon: 'error'
                    });
                }
            } catch (error) {
                Swal.fire({
                    title: 'Gagal membuat user',
                    text: 'Sebuah kesalahan telah terjadi. Silakan coba lagi atau hubungi administrator',
                    icon: 'error'
                });
            } finally {
                setIsSubmitting(false);
            }
        }
    }

    return (
        <div>
            <LoadingPage loading={isSubmitting}/>
            <BreadCrumbs href="/user" isChanged={isChanged} text="Kembali ke Daftar User"/>
            <h3>Buat akun</h3>
            <hr />
            <form onSubmit={onFormSubmit}>
                <fieldset disabled={isSubmitting}>
                    <div className='form-group mb-3'>
                        <label htmlFor="inputName" className="form-label fw-bold">Name <span className='text-danger'>*</span></label>
                        <input type="text" className="form-control" id="inputName" value={registerForm.name} onChange={(e) => onFormChange('name', e.target.value)}/>
                        <span className="text-danger small">{registerFormError.name.error}</span>
                    </div>
                    <div className='form-group mb-3'>
                        <label htmlFor="inputUsername" className="form-label fw-bold">Username <span className='text-danger'>*</span></label>
                        <input type="text" className="form-control" id="inputUsername" value={registerForm.username} onChange={(e) => onFormChange('username', e.target.value)}/>
                        <span className="text-danger small">{registerFormError.username.error}</span>
                    </div>
                    <div className='form-group mb-3'>
                        <label htmlFor="inputEmail" className="form-label fw-bold">Email <span className='text-danger'>*</span></label>
                        <input type="email" className="form-control" id="inputEmail" value={registerForm.email} onChange={(e) => onFormChange('email', e.target.value)}/>
                        <span className="text-danger small">{registerFormError.email.error}</span>
                    </div>
                    <div className='form-check mb-3'>
                        <input type="checkbox" className="form-check-input" id="inputRole" checked={registerForm.isAdmin} onChange={(e) => onRoleChange(e.target.checked)}/>
                        <label className="form-check-label" htmlFor="inputRole">is Admin</label>
                    </div>
                    <div className='form-group mb-3'>
                        <label htmlFor="inputPassword" className="form-label fw-bold">Password <span className='text-danger'>*</span></label>
                        <PasswordInput value={registerForm.password} onChange={(value) => onFormChange('password', value)}/>
                        <span className="text-danger small">{registerFormError.password.error}</span>
                    </div>
                    <div className='form-group mb-3'>
                        <label htmlFor="inputPassword" className="form-label fw-bold">Confirm Password <span className='text-danger'>*</span></label>
                        <PasswordInput value={registerForm.confirmPassword} onChange={(value) => onFormChange('confirmPassword', value)}/>
                        <span className="text-danger small">{registerFormError.confirmPassword.error}</span>
                    </div>
                    <br />
                    <input type="submit" className='btn btn-primary d-flex ms-auto' value='Daftar'/>
                </fieldset>
            </form>
        </div>
    );
}

export default function AccountPage() {
    return (
        <Authorize roleId={RoleEnum.ADMIN}>
            <Layout title='Account'>
                    <Account></Account>
            </Layout>
        </Authorize>
    );
}