import Joi from "joi";
import { GetServerSideProps } from "next";
import React, { useState } from "react"
import Swal from "sweetalert2";
import { UserClient } from "../../../../api/hiber-api";
import Authorize from "../../../shared/Authorize"
import BreadCrumbs from "../../../shared/components/BreadCrumbs";
import LoadingPage from "../../../shared/components/LoadingPage";
import Layout from "../../../shared/Layout"
import { PasswordInput } from "../../create";

interface ChangePasswordForm {
    currentPassword: string;
    newPassword: string;
    confirmNewPassword: string;
}

interface ErrorForm {
    currentPassword: {
        error: string
    };
    newPassword: {
        error: string
    };
    confirmNewPassword: {
        error: string
    };
}

const ChangePassword: React.FunctionComponent<{userId: string}> = (props) => {
    const [isSubmitting, setIsSubmitting] = useState<boolean>(false);
    const [isChanged, setIsChanged] = useState<boolean>(false);
    const [form, setForm] = useState<ChangePasswordForm>({
        confirmNewPassword: '',
        currentPassword: '',
        newPassword: ''
    });
    const [formError, setFormError] = useState<ErrorForm>({
        confirmNewPassword: {
            error: ''
        },
        currentPassword: {
            error: ''
        },
        newPassword: {
            error: ''
        }
    })

    const validateForm = (form, key?: string) => {
        const schema: {
            currentPassword: Joi.SchemaLike,
            newPassword: Joi.SchemaLike,
            confirmNewPassword: Joi.SchemaLike,
        } = {
            currentPassword: '',
            newPassword: '',
            confirmNewPassword: ''
        }

        const errorMessages = {
            currentPassword: '',
            newPassword: '',
            confirmNewPassword: ''
        };

        if (!key || key === 'currentPassword') {
            schema['currentPassword'] = Joi.string()
                .empty()
                .messages({
                    'string.empty': 'Password saat ini harus diisi',
                });
        }

        if (!key || key === 'newPassword') {
            schema['newPassword'] = Joi.string()
                .empty()
                .messages({
                    'string.empty': 'Password baru harus diisi',
                });
        }
        
        if (!key || key === 'confirmNewPassword') {
            schema['confirmNewPassword'] = Joi.string().empty().valid(Joi.ref('newPassword'))
                .messages({
                    'string.empty': 'Confirm password harus diisi',
                    'any.only': 'Confirm password harus sama dengan password'
                })
        }

        const validationResult = Joi.object(schema).validate(form, {
            abortEarly: false
        });

        const err = validationResult.error;
        
        if (!err){
            return undefined;
        }

        for (const detail of err.details){
            const key = detail.path[0]?.toString() ?? '';
            errorMessages[key] = detail.message;
        }

        return errorMessages;
    }

    const resetForm = () => {
        setForm({
            confirmNewPassword: '',
            currentPassword: '',
            newPassword: ''
        });
        setFormError({
            newPassword: {
                error: ''
            },
            confirmNewPassword: {
                error: ''
            },
            currentPassword: {
                error: ''
            },
        });
    }

    const onFormSubmit = async (e: React.ChangeEvent<HTMLFormElement>) => {
        e.preventDefault();
        const formToValidate = {
            newPassword: form.newPassword,
            confirmNewPassword: form.confirmNewPassword
        }

        const validationResult = validateForm(formToValidate);
        
        if(validationResult != undefined) {
            const newRegisterFormError: ErrorForm = formError;

            for (const key in newRegisterFormError) {
                if (validationResult && validationResult[key]) {
                    newRegisterFormError[key].error = validationResult[key];
                }
                else{
                    newRegisterFormError[key].error = '';
                }
            }
            
            setFormError({
                ...newRegisterFormError
            });
        }
        else {
            setIsSubmitting(true);
            try {
                const client = new UserClient();
                const response = await client.changePassword({
                    confirmNewPassword: form.confirmNewPassword,
                    newPassword: form.newPassword,
                    password: form.currentPassword
                }, props.userId);
                if(response == null) {
                    Swal.fire({
                        title: 'User tidak ditemukan',
                        text: 'Gagal mengubah data user',
                        icon: 'success'
                    });
                }
                else if(response == false) {
                    Swal.fire({
                        title: 'Gagal mengedit password user',
                        text: 'Password tidak sama dengan password anda saat ini',
                        icon: 'error'
                    });
                }
                else {
                    Swal.fire({
                        title: 'Berhasil mengedit password user',
                        text: 'Password berhasil diubah',
                        icon: 'success'
                    });

                    resetForm();
                }
            } catch (error) {
                Swal.fire({
                    title: 'Gagal mengedit info user',
                    text: 'Sebuah kesalahan telah terjadi. Silakan coba lagi atau hubungi administrator',
                    icon: 'error'
                });
            } finally {
                setIsSubmitting(false);
            }
        }
    }

    const onFormChange = (key: string, newValue: string) => {
        setIsChanged(true);

        const newForm = form;
        newForm[key] = newValue;

        const newErrorForm: ErrorForm = formError;
        const formToValidate = {
            currentPassword: form.currentPassword,
            confirmNewPassword: form.confirmNewPassword,
            newPassword: form.newPassword
        }

        const validationResult = validateForm(formToValidate, key);
        
        if (validationResult && validationResult[key]) {
            newErrorForm[key].error = validationResult[key];
        }
        else{
            newErrorForm[key].error = ''
        }
        setForm({
            ...newForm
        });
        setFormError({
            ...newErrorForm
        });
    }

    return (
        <div>
            <LoadingPage loading={isSubmitting}/>
            <BreadCrumbs href={`/user/view/${props.userId}`} isChanged={isChanged} text="Kembali ke lihat profil"/>
            <h3>Ubah Password</h3>
            <hr />
            <form onSubmit={onFormSubmit}>
                <fieldset disabled={isSubmitting}>
                    <div className='form-group mb-3'>
                        <label htmlFor="inputPassword" className="form-label fw-bold">Current Password</label>
                        <PasswordInput value={form.currentPassword ?? ''} onChange={(value) => onFormChange('currentPassword', value)}/>
                        <span className="text-danger small">{formError.currentPassword.error}</span>
                    </div>
                    <div className='form-group mb-3'>
                        <label htmlFor="inputPassword" className="form-label fw-bold">New Password</label>
                        <PasswordInput value={form.newPassword ?? ''} onChange={(value) => onFormChange('newPassword', value)}/>
                        <span className="text-danger small">{formError.newPassword.error}</span>
                    </div>
                    <div className='form-group mb-3'>
                        <label htmlFor="inputPassword" className="form-label fw-bold">Confirm New Password</label>
                        <PasswordInput value={form.confirmNewPassword ?? ''} onChange={(value) => onFormChange('confirmNewPassword', value)}/>
                        <span className="text-danger small">{formError.confirmNewPassword.error}</span>
                    </div>
                    <br />
                    <input type="submit" className='btn btn-primary d-flex ms-auto' value='Ubah Password'/>
                </fieldset>
            </form>
        </div>
    )
}

const ChangePasswordPage: React.FunctionComponent<{userId: string}> = (props) => {
    return (
        <Authorize onlyLoggedIn={true}>
            <Layout title={"Change Password"}>
                <ChangePassword userId={props.userId}></ChangePassword>
            </Layout>
        </Authorize>
    );
}

export const getServerSideProps: GetServerSideProps<{ userId: string }> = async (context) => {
    if (context.params) {
        const userId = context.params['userId'];

        if (typeof userId === 'string') {
            return {
                props: {
                    userId: userId
                }
            }
        }
    }

    return {
        props: {
            userId: ''
        }
    };
}

export default ChangePasswordPage;