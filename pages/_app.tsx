import type { AppProps } from 'next/app'
import { useEffect, useState } from 'react'
import React from 'react'
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';

import '../styles/globals.css';
import { useRouter } from 'next/dist/client/router';
import LoadingPage from './shared/components/LoadingPage';

function MyApp({ Component, pageProps }: AppProps) {
  const router = useRouter();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const handleStart = (url) => {
      url !== router.pathname ? setLoading(true) : setLoading(false);
    };
    const handleComplete = () => setLoading(false);

    router.events.on("routeChangeStart", handleStart);
    router.events.on("routeChangeComplete", handleComplete);
    router.events.on("routeChangeError", handleComplete);
  }, [router]);
  
  useEffect(() => {
    import('bootstrap');
  }, [])
  
  return (
    <div className='d-flex h-100'>
      <LoadingPage loading={loading}/>
      <Component {...pageProps} />
    </div>
  )
}

export default MyApp
