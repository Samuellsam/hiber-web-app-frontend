import { GetServerSideProps } from "next";
import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";
import { TransactionClient, TransactionDetailViewModel, TransactionProductDetail, PaymentHistoryViewModel,  ReturProductViewModel, PdfClient } from "../../api/hiber-api";
import Layout from "../shared/Layout";
import TransactionStatus from "../shared/components/transaction/transactionStatus";
import ReturStatus from "../shared/components/retur/returStatus";
import ReturTransactionModal from "../shared/components/retur/returTransactionModal";
import { PaymentTypeEnum, RoleEnum } from "../../enums/enums";
import Authorize from "../shared/Authorize";
import DeliveryStatus from "../shared/components/delivery/deliveryStatus";
import { DeliveryStatusEnum } from "../../enums/enums";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faDownload } from "@fortawesome/free-solid-svg-icons";
import { CopyClipboard } from "../shared/components/CopyClipboard";
import LoadingPage from "../shared/components/LoadingPage";
import PriceInput, { ReturnType } from "../shared/components/PriceInput";
import PriceFormat from "../shared/components/PriceFormat";
import BreadCrumbs from "../shared/components/BreadCrumbs";

const BOUGHT_PRODUCT = 1;
const PAYMENT_HISTORY = 2;
const RETUR_HISTORY = 3;

const TransactionDetailTabPanel: React.FunctionComponent<{
    listBoughtProduct: TransactionProductDetail[],
    listPaymentHistory: PaymentHistoryViewModel[],
    listReturHistory: ReturProductViewModel[]
}> = (props) => {
    const [currentPage, setCurrentPage] = useState<number>(1); 
    
    const renderBoughtProducts = () => {
        const list: TransactionProductDetail[] = props.listBoughtProduct as TransactionProductDetail[];

        if(list.length > 0) {
            return list.map(Q => 
                <tr key={Q.productId}>
                    <td>
                        <p>{Q.productName}</p>
                    </td>
                    <td>
                        <p>{Q.quantity}</p>
                    </td>
                    <td>
                        <p><PriceFormat value={Q.totalPrice}/></p>
                    </td>
                </tr>);
        }

        return (
            <tr>
                <td colSpan={3}>Tidak ada data</td>
            </tr>
        )
    }

    const renderPaymentHistory = () => {
        const list: PaymentHistoryViewModel[] = props.listPaymentHistory as PaymentHistoryViewModel[];
        
        if(list.length > 0) {
            return list.map((Q, index) => 
            <tr key={index}>
                <td>
                    <p>{new Date(Q.paymentDate).toLocaleDateString()}</p>
                </td>
                <td>
                    <p><PriceFormat value={Q.totalPayment}/></p>
                </td>
            </tr>);
        }

        return (
            <tr>
                <td colSpan={2}>Tidak ada data</td>
            </tr>
        )
    }

    const renderReturHistory = () => {
        const list: ReturProductViewModel[] = props.listReturHistory as ReturProductViewModel[];
        
        if(list.length > 0) {
            return list.map((Q, index) => 
            <tr key={index}>
                <td>
                    <p>{new Date(Q.createdAt).toLocaleDateString()}</p>
                </td>
                {Q.deliveryId ? 
                    <td className='d-flex justify-content-center'>
                        <p>{Q.deliveryId}</p>
                        <CopyClipboard text={Q.deliveryId}/>
                    </td> : <td>{'Tanpa pengiriman'}</td>}
                <td>
                    <p>{Q.productName}</p>
                </td>
                <td>
                    <p>{Q.productQuantity}</p>
                </td>
                <td>
                    <DeliveryStatus type={Q.deliveryStatusId}/>
                </td>
            </tr>);
        }

        return (
            <tr>
                <td colSpan={6}>Tidak ada data</td>
            </tr>
        )
    }

    const renderTabPanel = () => {
        if(currentPage == BOUGHT_PRODUCT) {
            return (
                <div>
                    <table className='table table-striped m-0 text-center'>
                        <thead className='table-header'>
                            <tr>
                                <th>Nama Produk</th>
                                <th>Kuantitas</th>
                                <th>Total Biaya</th>
                            </tr>
                        </thead>
                        <tbody>
                            {renderBoughtProducts()}
                        </tbody>
                    </table>
                </div>
            )
        }
        else if(currentPage == PAYMENT_HISTORY){
            return (
                <div>
                    <table className='table table-striped m-0 text-center'>
                        <thead className='table-header'>
                            <tr>
                                <th>Tanggal Pembayaran</th>
                                <th>Total Pembayaran</th>
                            </tr>
                        </thead>
                        <tbody>
                            {renderPaymentHistory()}
                        </tbody>
                    </table>
                </div>
            )
        }
        else if(currentPage == RETUR_HISTORY){
            return (
                <div>
                    <table className='table table-striped m-0 text-center'>
                        <thead className='table-header'>
                            <tr>
                                <th>Tanggal Retur</th>
                                <th>ID Pengiriman</th>
                                <th>Nama Produk</th>
                                <th>Kuantitas</th>
                                <th>Status Pengiriman</th>
                            </tr>
                        </thead>
                        <tbody>
                            {renderReturHistory()}
                        </tbody>
                    </table>
                </div>
            )
        } else {
            return undefined
        }
    }
    const getBoughtProductClassName = () => {
        if(currentPage == BOUGHT_PRODUCT) return "nav-link fw-bold me-2 selected-tab";

        return "nav-link text-light not-selected-tab me-2";
    }
    const getPaymentHistoryClassName = () => {
        if(currentPage == PAYMENT_HISTORY) return "nav-link fw-bold me-2 selected-tab";
        
        return "nav-link text-light not-selected-tab me-2 ";
    }
    const getReturHistoryClassName = () => {
        if(currentPage == RETUR_HISTORY) return "nav-link fw-bold me-2 selected-tab";
        
        return "nav-link text-light not-selected-tab me-2 ";
    }
    const changePanel = (newPage: number) => {
        setCurrentPage(newPage);
    }
    return (
        <div className='mx-auto'>
            <ul className="nav nav-tabs">
                <li className="nav-item">
                    <button className={getBoughtProductClassName()} onClick={() => changePanel(BOUGHT_PRODUCT)}>Barang Yang Dibeli</button>
                </li>
                <li className="nav-item">
                    <button className={getPaymentHistoryClassName()} onClick={() => changePanel(PAYMENT_HISTORY)}>Histori Pembayaran</button>
                </li>
                <li className="nav-item">
                    <button className={getReturHistoryClassName()} onClick={() => changePanel(RETUR_HISTORY)}>Histori Retur</button>
                </li>
            </ul>
            {renderTabPanel()}
        </div>
    );
}

const CreditPaymentModal: React.FunctionComponent<{
    transactionId: string,
    detail: TransactionDetailViewModel,
    setShowLoading: (isShow: boolean) => void,
    onPaid: () => void
}> = (props) => {
    const [payment, setPayment] = useState<string>('0');
    const [isPayButtonDisabled, setIsPayButtonDisabled] = useState<boolean>(true);
    const [errorInputPayment, setErrorInputPayment] = useState<string>('');
    const [isDirty, setIsDirty] = useState<boolean>(false);
    useEffect(() => {
        if(validatePayment()) {
            setIsPayButtonDisabled(false);
        }
        else {
            setIsPayButtonDisabled(true);
        }
    }, [payment])

    const validatePayment = () => {
        if(!isDirty) return false;
        if(payment.trim() == '') {
            setErrorInputPayment('Input tidak valid');
            return false;
        }
        else {
            const currPayment = parseFloat(payment);
            if(currPayment > (props.detail.totalPrice-props.detail.totalPayment)) {
                setErrorInputPayment('Jumlah lebih besar dari pada yang harus dibayar');
                return false;
            }
            else if(currPayment <= 0) {
                setErrorInputPayment('Input tidak valid');
                return false;
            }
            else {
                setErrorInputPayment('');
                return true;
            }
        }
    }
    const onChangePayment = (val: string) => {
        setIsDirty(true);
        setPayment(val);
    }

    const resetForm = () => {
        setPayment('0');
        setIsPayButtonDisabled(true);
        setErrorInputPayment('');
        setIsDirty(true);
        props.onPaid();
    }

    const pay = async () => {
        props.setShowLoading(true);
        const client = new TransactionClient();
        try {
            setIsPayButtonDisabled(true);
            const isSuccess: boolean = await client.payCreditTransaction(props.transactionId, {
                isLastPayment: parseFloat(payment) == (props.detail.totalPrice-props.detail.totalPayment) ? true : false,
                payment: parseFloat(payment)
            });
            Swal.fire({
                title: 'Berhasil melakukan pembayaran',
                text: 'Pembayaran berhasil',
                icon: 'success'
            });
            resetForm();
        } catch (error) {
            Swal.fire({
                title: 'Gagal melakukan pembayaran',
                text: 'Pembayaran gagal',
                icon: 'error'
            });
        } finally {
            setIsPayButtonDisabled(false);
            props.setShowLoading(false);
        }
    }

    return (
        <div className="modal fade" id="payStaticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex={-1} aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="staticBackdropLabel">Pembayaran</h5>
                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div className="modal-body">
                    <div className='d-flex flex-row justify-content-between'>
                        <p>Total Biaya</p>
                        <p><PriceFormat value={props.detail.totalPrice}/></p>
                    </div>
                    <div className='d-flex flex-row justify-content-between'>
                        <p>Sudah dibayar</p>
                        <p><PriceFormat value={props.detail.totalPayment}/></p>
                    </div>
                    <div className='d-flex flex-row justify-content-between'>
                        <p>Belum dibayar</p>
                        <p><PriceFormat value={props.detail.totalPrice-props.detail.totalPayment}/></p>
                    </div>
                    <div className='d-flex flex-row justify-content-between'>
                        <p>Bunga</p>
                        <p>{props.detail.interest+'%'}</p>
                    </div>
                    <br />
                    <hr />
                    <div className='flex-row d-flex justify-content-between'>
                        <p className='m-0 p-0'>Masukkan biaya yang akan dibayar</p>
                        <PriceInput value={parseInt(payment)} onChange={onChangePayment} className='w-100 text-end' returnType={ReturnType.STRING}/>
                    </div>
                    <p className='text-small text-danger text-center'>{errorInputPayment}</p>
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-secondary" data-bs-dismiss="modal" onClick={resetForm}>Batal</button>
                    <button type="button" className="btn btn-primary" disabled={isPayButtonDisabled} onClick={pay} data-bs-dismiss="modal">Bayar</button>
                </div>
                </div>
            </div>
        </div>
    );
}

const TransactionDetail: React.FunctionComponent<{transactionId: string}> = (props) => {
    const [detail, setDetail] = useState<TransactionDetailViewModel>({
        isPaid: false,
        paymentTypeName: '',
        totalPrice: 0,
        buyerName: '',
        totalPayment: 0,
        interest: 0,
        listBoughtProduct: [],
        listPaymentHistory: [],
        listReturHistory: [],
        deliveryStatusId: -1,
        paymentTypeId: -1
    });
    const [showReturButton, setShowReturButton] = useState<boolean>(false);
    const [isInRetur, setIsInRetur] = useState<boolean>(false);
    const [isLoading, setIsLoading] = useState<boolean>(true);

    useEffect(() => {
        fetchData();
    }, []);

    useEffect(() => {
        setReturButtonState();
        setReturStatusState();
    }, [detail])

    const setReturStatusState = () => {
        const list = detail.listReturHistory as ReturProductViewModel[];
        let isRetur = false;

        //cek list[0](delivery terakhir -> list[0] karena di backend dah di order desc created at) statusnya(kalo ada yg blm sampe jan ditampilin)
        if(list.length > 0 && list[0]?.deliveryStatusId != 3) {
            isRetur = true;
        }

        setIsInRetur(isRetur);
    }

    const setReturButtonState = () => {
        let isShow = false;

        if((detail.deliveryStatusId == DeliveryStatusEnum.BARANG_SAMPAI 
                    || detail.deliveryStatusId == undefined
                    || detail.deliveryStatusId == null) && 
            (detail.returDeliveryStatusId == DeliveryStatusEnum.BARANG_SAMPAI
                || detail.returDeliveryStatusId == undefined
                || detail.returDeliveryStatusId == null)) {
            isShow = true;
        }

        setShowReturButton(isShow);
    }

    const fetchData = async () => {
        setIsLoading(true);
        const client = new TransactionClient();
        try {
            const data: TransactionDetailViewModel = await client.getTransactionByTransactionId(props.transactionId);
            setDetail(data);
            
        } catch (error) {
            
        } finally {
            setIsLoading(false);
        }
    }

    const getTransactionFooter = () => {
        if(detail.isPaid) {
            if(showReturButton) {
                return (
                    <div className='d-flex'>
                        <ReturTransactionModal transactionId={props.transactionId} detail={detail} onRetur={fetchData} setShowLoading={isShow => setIsLoading(isShow)}/>
                        <button className='btn btn-info ms-auto my-auto' type="button" data-bs-toggle="modal" data-bs-target="#returStaticBackdrop">Retur</button>
                    </div>
                ) 
            }

            return undefined;
        }
        
        return (
            <div className='d-flex'>
                <CreditPaymentModal transactionId={props.transactionId} detail={detail} onPaid={fetchData} setShowLoading={isShow => setIsLoading(isShow)}/>
                <p className='ms-auto my-auto'>Biaya Yang Harus Dibayar: <PriceFormat value={detail.totalPrice - detail.totalPayment}/></p>
                <div className="col-1"></div>
                <button className='btn btn-primary my-auto' type="button" data-bs-toggle="modal" data-bs-target="#payStaticBackdrop">Bayar</button>
            </div>
        );
    }
    
    const base64ToArrayBuffer = (base64: string) => {
        var binary_string = window.atob(base64);
        var len = binary_string.length;
        var bytes = new Uint8Array(len);
        for (var i = 0; i < len; i++) {
            bytes[i] = binary_string.charCodeAt(i);
        }
        return bytes.buffer;
    }
    
    const printInstallment = async (transactionId: string) => {
        setIsLoading(true);
        const pdfClient = new PdfClient();
        try {
            const resultInstallment = await pdfClient.generateInstallmentApplication(transactionId);
            var blobInstallment = new Blob([base64ToArrayBuffer(resultInstallment)], { type: 'application/pdf' });
            var blobInstallmentURL = URL.createObjectURL(blobInstallment);
            window.open(blobInstallmentURL);

            Swal.fire({
                title: 'Berhasil mendownload template',
                text: 'Template cicilan berhasil di download',
                icon: 'success'
            });
        } catch (error) {
            Swal.fire({
                title: 'Gagal mendownload template',
                text: 'Template cicilan gagal di download',
                icon: 'error'
            });
        } finally {
            setIsLoading(false);
        }
    }
    
    return (
        <div>
            <LoadingPage loading={isLoading}/>
            <BreadCrumbs href="/transaction" isChanged={false} text="Kembali ke Daftar Transaksi"/>
            <h3 className="mb-4">Detail Transaksi</h3>
            <hr />
            <div className='row'>
                <div className='col-3'>
                    <p>ID Transaksi</p>
                </div>
                <div className='col-9 d-flex'>
                    <p>{props.transactionId}</p>
                    <CopyClipboard text={props.transactionId}/>
                </div>
            </div>
            {detail.deliveryId == undefined ? undefined : 
                <div className='row'>
                    <div className='col-3'>
                        <p>ID Pengiriman</p>
                    </div>
                    <div className='col-9 d-flex'>
                        <p>{detail.deliveryId}</p>
                        <CopyClipboard text={detail.deliveryId}/>
                    </div>
                </div>
            }
            <div className='row'>
                <div className='col-3'>
                    <p>Tanggal Transaksi</p>
                </div>
                <div className='col-9'>
                    <p>{detail.transactionDate}</p>
                </div>
            </div>
            <div className='row'>
                <div className='col-3'>
                    <p>Nama Pembeli</p>
                </div>
                <div className='col-9'>
                    <p>{detail.buyerName}</p>
                </div>
            </div>
            <div className='row'>
                <div className='col-3'>
                    <p>Metode Pembayaran</p>
                </div>
                <div className='col-9'>
                    <p>{detail.paymentTypeName}</p>
                </div>
            </div>
            <div className='row'>
                <div className='col-3'>
                    <p>Total Biaya Beserta Bunga</p>
                </div>
                <div className='col-9'>
                    <p><PriceFormat value={detail.totalPrice}/></p>
                </div>
            </div>
            <div className='row'>
                <div className='col-3'>
                    <p>Total Pembayaran</p>
                </div>
                <div className='col-9'>
                    <p><PriceFormat value={detail.totalPayment}/></p>
                </div>
            </div>
            <div className='row'>
                <div className='col-3'>
                    <p>Status Pembayaran</p>
                </div>
                <div className='col-9 d-flex'>
                    <TransactionStatus isPaid={detail.isPaid}></TransactionStatus>
                    {isInRetur ? <ReturStatus className='ms-2'/> : undefined}
                </div>
            </div>
            {detail.deliveryStatusId == undefined ? undefined : 
                <div className='row'>
                    <div className='col-3'>
                        <p>Status Pengiriman</p>
                    </div>
                    <div className='col-9 d-flex'>
                        <DeliveryStatus type={detail.deliveryStatusId}></DeliveryStatus>
                    </div>
                </div>
            }
            {detail.paymentTypeId == PaymentTypeEnum.KREDIT ? 
                <div className='row'>
                    <div className='col-3 d-flex'>
                        <p className='my-auto'>Pernyataan Cicilan</p>
                    </div>
                    <div className='col-9 d-flex'>
                        <button className='btn btn-success my-auto' type="button" onClick={() => printInstallment(props.transactionId)}><FontAwesomeIcon icon={faDownload} className='me-2'></FontAwesomeIcon>Download Pernyataan Cicilan</button>
                    </div>
                </div> : undefined
            }
            <br />
            <TransactionDetailTabPanel listBoughtProduct={detail.listBoughtProduct as TransactionProductDetail[]}
                                        listPaymentHistory={detail.listPaymentHistory as PaymentHistoryViewModel[]}
                                        listReturHistory={detail.listReturHistory as ReturProductViewModel[]}>
            </TransactionDetailTabPanel>
            <br />
            <br />
            {getTransactionFooter()}
        </div>
    );
}

const TransactionDetailPage: React.FunctionComponent<{transactionId: string}> = (props) => {
    return (
        <Authorize roleId={RoleEnum.ADMIN}>
            <Layout title='Transaction Detail'>
                <TransactionDetail transactionId={props.transactionId}></TransactionDetail>
            </Layout>
        </Authorize>
    );
}

export const getServerSideProps: GetServerSideProps<{ transactionId: string }> = async (context) => {
    if (context.params) {
        const transactionId = context.params['transactionId'];

        if (typeof transactionId === 'string') {
            return {
                props: {
                    transactionId: transactionId
                }
            }
        }
    }

    return {
        props: {
            transactionId: ''
        }
    };
}

export default TransactionDetailPage;