export enum DeliveryStatusEnum {
    MEMUAT_BARANG = 1,
    MENUJU_CUSTOMER,
    BARANG_SAMPAI
}

export enum RoleEnum {
    ADMIN = 1,
    EMPLOYEE
}

export enum PaymentTypeEnum {
    KREDIT = 1,
    TUNAI
}

export enum PageSizeEnum {
    A0 = 1,
    A1,
    A2,
    A3,
    A4,
    A6,
    A7,
    A8,
    A9,
    A10,
}

export enum PurchaseOrderStatusEnum {
    WAITING_FOR_APPROVAL = 1,
    DECLINED,
    APPROVED,
    PENGAJUAN_RETUR,
    SELESAI
}