export interface PageSizeModel {
    pageName: string;
    pageSize: string;
    selected: boolean;
    pageSizeEnum: number;
}

export var LIST_PAGE: PageSizeModel[] = [
    
    {
        pageName: 'A0',
        pageSize: '84.1 x 118.9 (cm)',
        selected: false,
        pageSizeEnum: 1
    },
    {
        pageName: 'A1',
        pageSize: '59.4 x 84.1 (cm)',
        selected: false,
        pageSizeEnum: 2
    },
    {
        pageName: 'A2',
        pageSize: '42.0 x 59.4 (cm)',
        selected: false,
        pageSizeEnum: 3
    },
    {
        pageName: 'A3',
        pageSize: '29.7 x 42.0 (cm)',
        selected: false,
        pageSizeEnum: 4
    },
    {
        pageName: 'A4',
        pageSize: '21.0 x 29.7 (cm)',
        selected: true,
        pageSizeEnum: 5
    },
    {
        pageName: 'A6',
        pageSize: '10.5 x 14.8 (cm)',
        selected: false,
        pageSizeEnum: 6
    },
    {
        pageName: 'A7',
        pageSize: '7.4 x 10.5 (cm)',
        selected: false,
        pageSizeEnum: 7
    },
    {
        pageName: 'A8',
        pageSize: '5.2 x 7.4 (cm)',
        selected: false,
        pageSizeEnum: 8
    },
    {
        pageName: 'A9',
        pageSize: '3.7 x 5.2 (cm)',
        selected: false,
        pageSizeEnum: 9
    },
    {
        pageName: 'A10',
        pageSize: '2.6 x 3.7 (cm)',
        selected: false,
        pageSizeEnum: 10
    },
]