export interface PaginationValue {
    currentPage: number;
    totalData: number;
    itemPerPage: number;
}